package persistence

import (
	"reflect"

	"gitlab.com/cosban/persistence/statement"
)

// Driver is an implementation of the methods necessary for persistence to fully interact with a dialect of SQL
type Driver interface {
	implementation
	// SetDefaultSchema overrides the default schema that would normally be set by the driver
	SetDefaultSchema(string)
	// DefaultSchema returns the schema that should be used for statements that do not specify a schema
	DefaultSchema() string
	// HasTable returns a statement that, when executed, specifies whether a table exists in the database
	HasTable(TableMeta) statement.Statement
	// CreateTable returns a statement that creates a table when executed
	CreateTable(TableMeta) statement.Statement
	// DropTable returns a statement that drops a table when executed
	DropTable(TableMeta) statement.Statement
	// GetTableColumns returns a statement that lists all available columns in a specific table when executed
	GetTableColumns(TableMeta) statement.Statement
	// AddTableColumn returns a statement that alters the specified table to include an additional column when executed
	AddTableColumn(TableMeta, FieldMeta) statement.Statement
	// Query returns a statement that can be used to execute a database query
	Query(TableMeta, QueryPieces) statement.Statement
	// Insert returns a statement that can be used to insert data
	Insert(TableMeta) statement.Statement
	// Update returns a statement that can be used to update data
	Update(TableMeta, ...Constraint) statement.Statement
	// Upsert returns a statement that can be used to insert data if it does not exist, or update it in the event that
	// it does
	Upsert(TableMeta) statement.Statement
	// Delete returns a statement that can be used to delete data
	Delete(TableMeta, ...Constraint) statement.Statement
	// GetFullTableName returns the fully qualified name (typically [database].[schema].[table]) of a table
	GetFullTableName(TableMeta) string
	// Select is used to create the select portion of a query from the given fields
	Select([]string) string
}

type implementation interface {
	// Bind is used to return the string that should be used to bind variables in a prepared statement when the given
	// number of already bound variables is passed in
	Bind(int) string
	// IsValidType returns whether a type may be used by the implementing driver
	IsValidType(reflect.Type) bool

	ToColumnType(column FieldMeta) string

	RowIDField() FieldMeta
}
