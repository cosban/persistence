package persistence

import (
	"bytes"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/cosban/persistence/statement"
)

var (
	joinKinds = map[JoinKind]string{
		InnerJoin:      "INNER JOIN",
		RightOuterJoin: "RIGHT OUTER JOIN",
		LeftOuterJoin:  "LEFT OUTER JOIN",
		FullOuterJoin:  "FULL OUTER JOIN",
	}
)

func toStringList(columns *FieldMetas) string {
	var buffer bytes.Buffer
	prefix := ""
	for i := 0; i < columns.Size(); i++ {
		_, column := columns.Index(i)
		buffer.WriteString(prefix)
		buffer.WriteString(column.GetName())
		prefix = ", "
	}
	return buffer.String()
}

// CommonDriver contains a base functionality for many sql driver operations which may be used or overridden by any
// dialect driver. It is not mandatory that a dialect driver makes use of the common driver even though all drivers must
// implement the driver interface.
type CommonDriver struct {
	schema       string
	bind         func(int) string
	isValidType  func(reflect.Type) bool
	toColumnType func(FieldMeta) string
}

// SetBindFunc is to be used by all dialect drivers which use the CommonDriver. It specifies how to bind variables
// within a query
func (d *CommonDriver) SetBindFunc(fn func(int) string) {
	d.bind = fn
}

// SetIsValidTypeFunc is to be used by all dialect drivers which use the CommonDriver. It specifies how to determine
// whether a provided type is valid
func (d *CommonDriver) SetIsValidTypeFunc(fn func(reflect.Type) bool) {
	d.isValidType = fn
}

// SetToColumnTypeFunc is to be used by all dialect drivers which use the CommonDriver. It specifies how to determine
// the column type to use for any given field to be used in the database.
func (d *CommonDriver) SetToColumnTypeFunc(fn func(FieldMeta) string) {
	d.toColumnType = fn
}

// SetDefaultSchema sets a default schema for all operations to use unless overridden by the operation.
func (d *CommonDriver) SetDefaultSchema(schema string) {
	d.schema = schema
}

// DefaultSchema returns the default schema which is to be used for all operations unless overridden.
func (d *CommonDriver) DefaultSchema() string {
	return d.schema
}

// GetSchema returns the schema which has been specified by a TableMeta, if none is provided, the default will be used
func (d *CommonDriver) GetSchema(meta TableMeta) string {
	schema := d.DefaultSchema()
	if meta.Schema != "" {
		schema = meta.Schema
	}
	return schema
}

// GetFullTableName returns the fully qualified name for a table. This means that the schema is included.
func (d *CommonDriver) GetFullTableName(meta TableMeta) string {
	schema := d.GetSchema(meta)
	if schema == "" {
		return meta.Name
	}
	return fmt.Sprintf("%s.%s", schema, meta.Name)
}

func (d *CommonDriver) BuildRepeatableSection(
	prefix, joiner string,
	meta TableMeta,
	argsLen int,
	qualifyColumn bool,
	shouldParse func(FieldMeta) bool,
) (string, []interface{}) {
	var buffer bytes.Buffer
	var args []interface{}
	columns := meta.Columns.Iter()
	for columns.Next() {
		_, column := columns.Current()
		if !shouldParse(column) {
			continue
		}
		args = append(args, column.Value())
		bind := d.Bind(len(args) + argsLen)
		buffer.WriteString(prefix)
		prefix = joiner
		if qualifyColumn {
			buffer.WriteString(d.qualifyColumn(meta, column.GetName()))
		} else {
			buffer.WriteString(column.GetName())
		}
		buffer.WriteString(" = ")
		buffer.WriteString(bind)
	}
	return buffer.String(), args
}

// Constraints implements the basic functionality to create a list of constraints which are to be used in queries for
// most dialects
func (d *CommonDriver) Constraints(constraints []Constraint, currentArgs int) (string, []interface{}) {
	args := []interface{}{}
	var buffer bytes.Buffer
	for _, constraint := range constraints {
		for _, arg := range constraint.Args {
			args = append(args, arg)
			bound := d.Bind(currentArgs + len(args))
			constraint.Section = strings.Replace(constraint.Section, "?", bound, 1)
		}
		prefix := fmt.Sprintf(" %s ", constraint.Kind)
		buffer.WriteString(prefix)
		buffer.WriteString(constraint.Section)
	}
	return buffer.String(), args
}

// DeriveConstraints is used to determine any constraints that a column may have on it during table creation or the
// addition of columns. This includes uniqueness, primary key, and combined key constraints.
func (d *CommonDriver) DeriveConstraints(columns *FieldMetas) string {
	var buffer bytes.Buffer
	combined := map[string][]string{}
	primary := []string{}
	for i := 0; i < columns.Size(); i++ {
		_, column := columns.Index(i)
		if column.Tag.CombinedKey != "" {
			group := combined[column.Tag.CombinedKey]
			if group == nil {
				group = []string{}
			}
			combined[column.Tag.CombinedKey] = append(group, column.GetName())
		}
		if column.Tag.PrimaryKey {
			primary = append(primary, column.GetName())
		}
	}
	prefix := ", UNIQUE("
	suffix := ""
	for _, columns := range combined {
		for _, column := range columns {
			buffer.WriteString(prefix)
			buffer.WriteString(column)
			prefix = ", "
		}
		buffer.WriteString(")")
		prefix = ", UNIQUE("
	}
	prefix = ", PRIMARY KEY ("
	for _, column := range primary {
		buffer.WriteString(prefix)
		buffer.WriteString(column)
		prefix = ", "
		suffix = ")"
	}
	buffer.WriteString(suffix)
	return buffer.String()
}

// CreateTable implements the basic functionality to create a table in the database for most dialects
func (d *CommonDriver) CreateTable(meta TableMeta) statement.Statement {
	columns := d.ColsToString(meta.Columns)
	constraints := d.DeriveConstraints(meta.Columns)
	return Prepare(fmt.Sprintf(
		"CREATE TABLE %s (%s%s);",
		d.GetFullTableName(meta), columns, constraints,
	))
}

// AddTableColumn implements the basic functionality to add a column to a table in the database for most dialects
func (d *CommonDriver) AddTableColumn(meta TableMeta, field FieldMeta) statement.Statement {
	fields := NewFieldMetasFrom(field)
	return Prepare(
		fmt.Sprintf(`ALTER TABLE %s ADD COLUMN %s`, d.GetFullTableName(meta), d.ColsToString(fields)),
	)
}

// DropTable implements the basic functionality to drop a table from the database for most dialects
func (d *CommonDriver) DropTable(model TableMeta) statement.Statement {
	return Prepare(fmt.Sprintf("DROP TABLE %s;", d.GetFullTableName(model)))
}

// Select implements the basic functionality to provide the selected columns portion of a query for most dialects
func (d *CommonDriver) Select(selects []string) string {
	var buffer bytes.Buffer
	prefix := "SELECT "
	for _, selector := range selects {
		buffer.WriteString(prefix)
		buffer.WriteString(selector)
		prefix = ", "
	}
	return buffer.String()
}

// Query implements the basic functionality to create a full query in most dialects
func (d *CommonDriver) Query(meta TableMeta, pieces QueryPieces) statement.Statement {
	var buffer bytes.Buffer
	buffer.WriteString(d.Select(pieces.Selects))
	buffer.WriteString(" FROM ")
	buffer.WriteString(d.GetFullTableName(meta))
	join, joinArgs := d.JoinConstraints(pieces.Joins, len(pieces.Args))
	pieces.Args = append(pieces.Args, joinArgs...)
	buffer.WriteString(join)
	filter, filterArgs := d.Constraints(pieces.Filters, len(pieces.Args))
	pieces.Args = append(pieces.Args, filterArgs...)
	buffer.WriteString(filter)
	prefix := " ORDER BY "
	for _, order := range pieces.Order {
		buffer.WriteString(prefix)
		buffer.WriteString(order)
		prefix = ", "
	}
	if pieces.Limit > 0 {
		buffer.WriteString(" LIMIT ")
		buffer.WriteString(strconv.Itoa(pieces.Limit))
	}

	return PrepareQuery(buffer.String(), pieces.Args...)
}

// Insert implements the basic functionality to insert one or more items of the same type into a table for most dialects
func (d *CommonDriver) Insert(meta TableMeta) statement.Statement {
	var args []interface{}
	var buffer bytes.Buffer
	buffer.WriteString("INSERT INTO ")
	buffer.WriteString(d.GetFullTableName(meta))
	var columns []string
	var values []interface{}
	cols := meta.Columns.Iter()
	for cols.Next() {
		_, column := cols.Current()
		if column.Set() {
			columns = append(columns, column.GetName())
			values = append(values, column.Value())
		}
	}
	precol := " ("
	preval := ") VALUES ("
	var colbuf bytes.Buffer
	var valbuf bytes.Buffer
	for i := range columns {
		args = append(args, values[i])
		bind := d.Bind(len(args))
		colbuf.WriteString(precol)
		colbuf.WriteString(columns[i])
		valbuf.WriteString(preval)
		valbuf.WriteString(bind)
		precol = ", "
		preval = ", "
	}
	buffer.WriteString(colbuf.String())
	buffer.WriteString(valbuf.String())
	buffer.WriteString(")")
	return Prepare(buffer.String(), args...)
}

// Update implements the basic functionality to update currently existing values in the database for most dialects
func (d *CommonDriver) Update(meta TableMeta, constraints ...Constraint) statement.Statement {
	update, args := d.update(meta, constraints, 0, true)
	return Prepare(update, args...)
}

func (d *CommonDriver) update(
	meta TableMeta,
	constraints []Constraint,
	arglen int,
	includeTableName bool,
) (string, []interface{}) {
	var buffer bytes.Buffer
	buffer.WriteString("UPDATE")
	if includeTableName {
		buffer.WriteString(" ")
		buffer.WriteString(d.GetFullTableName(meta))
	}
	updates, args := d.BuildRepeatableSection(" SET ", ", ", meta, arglen, false, func(column FieldMeta) bool {
		return !column.Tag.Generated && !column.Tag.PrimaryKey
	})
	buffer.WriteString(updates)
	filter, fArgs := d.Constraints(constraints, len(args)+arglen)
	buffer.WriteString(filter)
	args = append(args, fArgs...)

	prefix := " WHERE "
	if len(constraints) > 0 {
		prefix = " AND "
	}
	metaConstraints, conArgs := d.BuildRepeatableSection(prefix, " AND ", meta, len(args)+arglen, true, func(column FieldMeta) bool {
		return column.Tag.PrimaryKey && column.Set()
	})
	buffer.WriteString(metaConstraints)
	args = append(args, conArgs...)

	return buffer.String(), args
}

// Upsert implements the basic functionality to insert or update (if already existing) values for most dialects
func (d *CommonDriver) Upsert(meta TableMeta) statement.Statement {
	var buffer bytes.Buffer
	var args []interface{}

	for i := 0; i < meta.Primary.Size(); i++ {
		_, column := meta.Primary.Index(i)
		if column.Zero() {
			return d.Insert(meta)
		}
	}
	insert := d.Insert(meta)
	args = append(args, insert.Args()...)
	buffer.WriteString(insert.Query())
	buffer.WriteString(" ON CONFLICT (")
	buffer.WriteString(toStringList(meta.Primary))
	buffer.WriteString(") DO ")
	if meta.AreAllColumnsPrimary() {
		buffer.WriteString("NOTHING")
	} else {
		update, upArgs := d.update(meta, nil, len(args), false)
		args = append(args, upArgs...)
		buffer.WriteString(update)
	}
	return Prepare(buffer.String(), args...)
}

// Delete implements the basic functionality to delete entries from a table for most dialects
func (d *CommonDriver) Delete(meta TableMeta, constraints ...Constraint) statement.Statement {
	var buffer bytes.Buffer
	buffer.WriteString("DELETE FROM ")
	buffer.WriteString(d.GetFullTableName(meta))

	filter, args := d.Constraints(constraints, 0)
	buffer.WriteString(filter)
	prefix := " WHERE "
	if len(args) > 0 {
		prefix = " AND "
	}
	metaConstraints, metaArgs := d.BuildRepeatableSection(prefix, " AND ", meta, len(args), true, func(column FieldMeta) bool {
		return column.Set() && column.Tag.PrimaryKey
	})
	buffer.WriteString(metaConstraints)
	args = append(args, metaArgs...)
	return Prepare(buffer.String(), args...)
}

// ColsToString implements the basic functionality to provide basic database type information for all fields a table
// will have for table creation, or gathering metadata about the table for most dialects
func (d *CommonDriver) ColsToString(columns *FieldMetas) string {
	var buffer bytes.Buffer
	prefix := ""
	for i := 0; i < columns.Size(); i++ {
		_, column := columns.Index(i)
		kind := d.ToColumnType(column)
		if kind == "" {
			continue
		}
		buffer.WriteString(prefix)
		buffer.WriteString(column.GetName())
		buffer.WriteString(" ")
		buffer.WriteString(kind)
		if column.Tag.NotNullable {
			buffer.WriteString(" NOT NULL")
		}
		if column.Tag.Unique {
			buffer.WriteString(" UNIQUE")
		}
		if column.Tag.DefaultValue != "" {
			buffer.WriteString(" DEFAULT ")
			buffer.WriteString(column.Tag.DefaultValue)
		}
		if column.Tag.References != "" {
			buffer.WriteString(" REFERENCES ")
			buffer.WriteString(column.Tag.References)
			buffer.WriteString(" ON DELETE CASCADE")
		}

		prefix = ", "
	}
	return buffer.String()
}

func (d *CommonDriver) JoinConstraints(constraints []JoinConstraint, currentArgs int) (string, []interface{}) {
	args := []interface{}{}
	var buffer bytes.Buffer
	for _, constraint := range constraints {
		joiner, joinArgs := d.Join(constraint, len(args)+currentArgs)
		buffer.WriteString(joiner)
		args = append(args, joinArgs...)
	}
	return buffer.String(), args
}

// Join implements the basic functionality to join against another table within a query for most dialects.
func (d *CommonDriver) Join(constraint JoinConstraint, currentArgs int) (string, []interface{}) {
	args := []interface{}{}
	from := constraint.From
	against := constraint.Against
	var buffer bytes.Buffer
	kind := joinKinds[constraint.Kind]
	buffer.WriteString(fmt.Sprintf(" %s ", kind))
	againstName := d.GetFullTableName(against)
	buffer.WriteString(againstName)
	if constraint.Section != "" {
		buffer.WriteString(" ON ")
		for _, arg := range constraint.Args {
			bind := d.Bind(len(args))
			constraint.Section = strings.Replace(constraint.Section, "?", bind, 1)
			args = append(args, arg)
		}
		buffer.WriteString(constraint.Section)
		args = append(args, constraint.Args...)
	} else {
		prefix := " ON "
		for i := 0; i < against.Primary.Size(); i++ {
			name, column := against.Primary.Index(i)
			if against.JoinTable {
				name = strings.TrimPrefix(name, from.Name+"_")
			}
			if from.JoinTable {
				name = against.Name + "_" + name
			}
			fromColumn := from.Columns.Get(name)
			if fromColumn.Valid {
				buffer.WriteString(prefix)
				againstCol := d.qualifyColumn(against, column.GetName())
				buffer.WriteString(againstCol)
				buffer.WriteString(" = ")
				fromCol := d.qualifyColumn(from, fromColumn.GetName())
				buffer.WriteString(fromCol)
				prefix = " AND "
			}
		}
	}
	return buffer.String(), args
}

func (d *CommonDriver) qualifyColumn(meta TableMeta, name string) string {
	return fmt.Sprintf("%s.%s", d.GetFullTableName(meta), name)
}

// Bind calls the bind function which was previously provided by the dialect driver
func (d *CommonDriver) Bind(length int) string {
	return d.bind(length)
}

// IsValidType calls the isValidType function which was previously provided by the dialect driver
func (d *CommonDriver) IsValidType(t reflect.Type) bool {
	return d.isValidType(t)
}

// ToColumnType calls the toColumnType function which was previously provided by the dialect driver
func (d *CommonDriver) ToColumnType(column FieldMeta) string {
	return d.toColumnType(column)
}
