package persistence

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
)

func TestNoSchemasGiven(t *testing.T) {
	driver := CommonDriver{}
	given := TableMeta{}
	actual := driver.GetSchema(given)
	assert.Equals(t, "", actual)
}

func TestSchemaOnDriverOnly(t *testing.T) {
	expected := "test"
	driver := CommonDriver{}
	driver.SetDefaultSchema(expected)
	given := TableMeta{}
	actual := driver.GetSchema(given)
	assert.Equals(t, expected, actual)
}

func TestSchemaOnMetaOnly(t *testing.T) {
	expected := "test"
	driver := CommonDriver{}
	given := TableMeta{
		Schema: expected,
	}
	actual := driver.GetSchema(given)
	assert.Equals(t, expected, actual)
}

func TestSchemaMetaOverridesDriver(t *testing.T) {
	expected := "meta"
	unexpected := "driver"
	driver := CommonDriver{}
	driver.SetDefaultSchema(unexpected)
	given := TableMeta{
		Schema: expected,
	}
	actual := driver.GetSchema(given)
	assert.Equals(t, expected, actual)
}

func TestGetFullTableNameNoSchema(t *testing.T) {
	driver := CommonDriver{}
	expected := "expected"

	given := TableMeta{
		Name: expected,
	}

	actual := driver.GetFullTableName(given)
	assert.Equals(t, expected, actual)
}

func TestGetFullTableNameWitchSchema(t *testing.T) {
	driver := CommonDriver{}

	given := TableMeta{
		Name:   "name",
		Schema: "schema",
	}

	actual := driver.GetFullTableName(given)
	expected := "schema.name"
	assert.Equals(t, expected, actual)
}

func TestQualifyColumnNameWithSchema(t *testing.T) {
	driver := CommonDriver{}

	given := TableMeta{
		Name:   "name",
		Schema: "schema",
		Columns: NewFieldMetasFrom(
			FieldMeta{
				Name: "column",
			},
		),
	}

	actual := driver.qualifyColumn(given, "column")
	expected := "schema.name.column"
	assert.Equals(t, expected, actual)
}

func TestQualifyColumnNameNoSchema(t *testing.T) {
	driver := CommonDriver{}

	given := TableMeta{
		Name: "name",
		Columns: NewFieldMetasFrom(
			FieldMeta{
				Name: "column",
			},
		),
	}

	actual := driver.qualifyColumn(given, "column")
	expected := "name.column"
	assert.Equals(t, expected, actual)
}

func TestCreateTable(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	given := ObtainTableMeta(table{}, mockValidator)
	actual := driver.CreateTable(given)
	expected := Prepare("CREATE TABLE persist.table (id SERIAL, Flag BOOLEAN NOT NULL, deleted TIMESTAMP, Updated TIMESTAMP NOT NULL DEFAULT now(), PRIMARY KEY (id));")
	assert.Equals(t, expected, actual)
}

func TestDropTable(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	given := ObtainTableMeta(table{}, mockValidator)
	actual := driver.DropTable(given)
	expected := Prepare("DROP TABLE persist.table;")
	assert.Equals(t, expected, actual)
}

func TestDeleteEmptyModelWithConstraints(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	given := ObtainTableMeta(table{}, mockValidator)

	constraints := []Constraint{
		{
			Section: "Flag = ?",
			Kind:    "WHERE",
			Args:    []interface{}{true},
		},
	}
	actual := driver.Delete(given, constraints...)

	expected := Prepare("DELETE FROM persist.table WHERE Flag = ?", true)
	assert.Equals(t, expected, actual)
}

func TestDeletedModelWithConstraints(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	constraints := []Constraint{
		{
			Section: "Flag = ?",
			Kind:    "WHERE",
			Args:    []interface{}{true},
		},
	}

	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{ID: 12}, mockValidator)
	actual := driver.Delete(meta, constraints...)

	expected := Prepare("DELETE FROM persist.table WHERE Flag = ? AND persist.table.id = ?", true, 12)
	assert.Equals(t, expected, actual)
}

func TestDeleteModelWithNoConstraints(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{ID: 12}, mockValidator)
	actual := driver.Delete(meta)

	expected := Prepare("DELETE FROM persist.table WHERE persist.table.id = ?", 12)
	assert.Equals(t, expected, actual)
}

func TestUpdateModelWithConstraint(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{Flag: false}, mockValidator)
	constraints := []Constraint{
		{
			Section: "Flag = ?",
			Kind:    "WHERE",
			Args:    []interface{}{true},
		},
	}
	actual := driver.Update(meta, constraints...)

	expected := Prepare("UPDATE persist.table SET Flag = ?, deleted = ? WHERE Flag = ?", false, (*time.Time)(nil), true)
	assert.Equals(t, expected, actual)
}

func TestUpdateSpecificModel(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	combinedMeta := ObtainTableMeta(table{ID: 12, Flag: false}, mockValidator)
	actual := driver.Update(combinedMeta)
	expected := Prepare("UPDATE persist.table SET Flag = ?, deleted = ? WHERE persist.table.id = ?", false, (*time.Time)(nil), 12)
	assert.Equals(t, expected, actual)
}

func TestInsertModel(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	combinedMeta := ObtainTableMeta(table{ID: 12, Flag: false}, mockValidator)
	actual := driver.Insert(combinedMeta)
	expected := Prepare("INSERT INTO persist.table (id, Flag) VALUES (?, ?)", 12, false)
	assert.Equals(t, expected, actual)
}

func TestUpsertModel(t *testing.T) {
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}

	driver := mockedCommonDriver()
	combinedMeta := ObtainTableMeta(table{ID: 12, Flag: false}, mockValidator)
	actual := driver.Upsert(combinedMeta)

	expected := Prepare(
		"INSERT INTO persist.table (id, Flag) VALUES (?, ?) ON CONFLICT (id) DO UPDATE SET Flag = ?, deleted = ? WHERE persist.table.id = ?",
		12, false, false, (*time.Time)(nil), 12,
	)
	assert.Equals(t, expected, actual)
}

func TestQuery(t *testing.T) {
	assert := assert.New(t)
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	pieces := QueryPieces{
		Schema:  "persist",
		Table:   "table",
		Selects: []string{"id"},
		Filters: []Constraint{
			{
				Section: "Flag = ?",
				Kind:    "WHERE",
				Args:    []interface{}{true},
			},
		},
		Order: []string{"id desc"},
	}
	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{}, mockValidator)

	actual := driver.Query(meta, pieces)

	expected := PrepareQuery("SELECT id FROM persist.table WHERE Flag = ? ORDER BY id desc", true)
	assert.Equals(expected, actual)
}

func TestJoinQuery(t *testing.T) {
	assert := assert.New(t)
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	type otherTable struct {
		ID int `persistence:"primary_key,name:id"`
	}
	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{}, mockValidator)

	otherMeta := ObtainTableMeta(otherTable{}, mockValidator)

	pieces := QueryPieces{
		Schema:  "persist",
		Table:   "table",
		Selects: []string{"id"},
		Joins: []JoinConstraint{
			JoinConstraint{
				Kind:    InnerJoin,
				From:    meta,
				Against: otherMeta,
			},
		},
	}

	actual := driver.Query(meta, pieces)

	expected := PrepareQuery("SELECT id FROM persist.table INNER JOIN persist.otherTable ON persist.otherTable.id = persist.table.id")
	assert.Equals(expected, actual)
}

func TestMultiJoinQuery(t *testing.T) {
	assert := assert.New(t)
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	type otherTable struct {
		ID int `persistence:"primary_key,name:id"`
	}
	driver := mockedCommonDriver()
	meta := ObtainTableMeta(table{}, mockValidator)
	middleFieldMetas := NewFieldMetasFrom(
		FieldMeta{Name: "table_ID", Tag: Tag{Valid: true}, Valid: true},
		FieldMeta{Name: "otherTable_ID", Tag: Tag{Valid: true}, Valid: true},
	)
	middleMeta := TableMeta{
		Name:      "table_otherTable",
		Schema:    "persist",
		JoinTable: true,
		Columns:   middleFieldMetas,
		Primary:   middleFieldMetas,
	}
	otherMeta := ObtainTableMeta(otherTable{}, mockValidator)

	pieces := QueryPieces{
		Schema:  "persist",
		Table:   "table",
		Selects: []string{"id", "joinId", "value"},
		Joins: []JoinConstraint{
			JoinConstraint{
				Kind:    InnerJoin,
				From:    meta,
				Against: middleMeta,
			},
			JoinConstraint{
				Kind:    InnerJoin,
				From:    middleMeta,
				Against: otherMeta,
			},
		},
	}

	actual := driver.Query(meta, pieces)

	expected := PrepareQuery("SELECT id, joinId, value FROM persist.table INNER JOIN persist.table_otherTable ON persist.table_otherTable.table_ID = persist.table.id INNER JOIN persist.otherTable ON persist.otherTable.id = persist.table_otherTable.otherTable_ID")

	assert.Equals(expected, actual)
}

func mockedCommonDriver() CommonDriver {
	return CommonDriver{
		bind:         mockValidator.Bind,
		isValidType:  mockValidator.IsValidType,
		toColumnType: mockValidator.ToColumnType,
	}
}

var mockValidator = mockTypeValidator{}

type mockTypeValidator struct{}

func (mockTypeValidator) Bind(i int) string {
	return "?"
}

func (mockTypeValidator) IsValidType(t reflect.Type) bool {
	if t.Kind() != reflect.Ptr {
		t = reflect.New(t).Type()
	}
	typesToColumn := map[reflect.Type]string{
		reflect.TypeOf((*bool)(nil)):      "BOOLEAN",
		reflect.TypeOf((*int16)(nil)):     "SMALLINT",
		reflect.TypeOf((*int32)(nil)):     "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "BIGINT",
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*uint32)(nil)):    "INTEGER",
		reflect.TypeOf((*uint64)(nil)):    "BIGINT",
		reflect.TypeOf((*uint)(nil)):      "INTEGER",
		reflect.TypeOf((*string)(nil)):    "TEXT",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP",
	}
	_, ok := typesToColumn[t]
	return ok
}

func (mockTypeValidator) ToColumnType(column FieldMeta) string {
	generatedTypes := map[reflect.Type]string{
		reflect.TypeOf((*int)(nil)):       "SERIAL",
		reflect.TypeOf((*int16)(nil)):     "SMALLSERIAL",
		reflect.TypeOf((*int32)(nil)):     "SERIAL",
		reflect.TypeOf((*int64)(nil)):     "BIGSERIAL",
		reflect.TypeOf((*uint)(nil)):      "SERIAL",
		reflect.TypeOf((*uint16)(nil)):    "SMALLSERIAL",
		reflect.TypeOf((*uint32)(nil)):    "SERIAL",
		reflect.TypeOf((*uint64)(nil)):    "BIGSERIAL",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP NOT NULL DEFAULT now()",
	}
	t := column.Type
	if column.Type.Kind() != reflect.Ptr {
		t = reflect.New(column.Type).Type()
	}
	if column.Tag.Generated {
		return generatedTypes[t]
	}
	typesToColumn := map[reflect.Type]string{
		reflect.TypeOf((*bool)(nil)):      "BOOLEAN",
		reflect.TypeOf((*int16)(nil)):     "SMALLINT",
		reflect.TypeOf((*int32)(nil)):     "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "BIGINT",
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*uint32)(nil)):    "INTEGER",
		reflect.TypeOf((*uint64)(nil)):    "BIGINT",
		reflect.TypeOf((*uint)(nil)):      "INTEGER",
		reflect.TypeOf((*string)(nil)):    "TEXT",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP",
	}
	if column.Tag.Type == "" {
		return typesToColumn[t]
	}
	return column.Tag.Type
}

func (mockTypeValidator) GetSchema() string {
	return "persist"
}
