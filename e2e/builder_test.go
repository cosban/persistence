package e2e

import (
	"testing"
	"time"

	"github.com/cosban/assert"
	"gitlab.com/cosban/persistence"
	"gitlab.com/cosban/persistence/internal"
	_ "gitlab.com/cosban/persistence/sqlite3"
)

func TestParentChild(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	defer c.Close()
	assert.Nil(t, err)
	type tableInfo struct {
		Name    string
		Type    string
		NotNull bool
		Primary bool
	}
	type foreignKeyInfo struct {
		Table string
		From  string
		To    string
	}
	type child struct {
		ID     int64  `persistence:"primary_key,generated"`
		Parent int64  `persistence:"references:parent(ID)"`
		Value  string `persistence:"not_null"`
	}
	type parent struct {
		ID       int64 `persistence:"primary_key,generated"`
		Name     string
		Children []*child
	}
	scanColumns := func(table string, t *testing.T) []tableInfo {
		stmt := persistence.PrepareQuery(`SELECT name, type, [notnull], pk FROM pragma_table_info('` + table + `') ORDER BY cid;`)
		rows, err := c.RawStatement().Query(stmt)
		assert.Nil(t, err)
		defer rows.Close()
		actual := []tableInfo{}
		for rows.Next() {
			info := tableInfo{}
			err := rows.Scan(&info.Name, &info.Type, &info.NotNull, &info.Primary)
			assert.Nil(t, err)
			actual = append(actual, info)
		}
		return actual
	}
	scanForeignKeys := func(table string, t *testing.T) []foreignKeyInfo {
		stmt := persistence.PrepareQuery(`SELECT [table], [from], [to] FROM pragma_foreign_key_list('` + table + `') ORDER BY id;`)
		rows, err := c.RawStatement().Query(stmt)
		assert.Nil(t, err)
		defer rows.Close()
		actual := []foreignKeyInfo{}
		for rows.Next() {
			info := foreignKeyInfo{}
			err := rows.Scan(&info.Table, &info.From, &info.To)
			assert.Nil(t, err)
			actual = append(actual, info)
		}
		assert.Nil(t, rows.Err())
		return actual
	}
	t.Run("TestCreateTables", func(t *testing.T) {
		err = c.BuildStatement().CreateFromModel(parent{}, child{})
		assert.Nil(t, err)
	})
	t.Run("TestCorrectParentTableColumns", func(t *testing.T) {
		actual := scanColumns("parent", t)
		expected := []tableInfo{
			{Name: "ID", Type: "INTEGER", NotNull: false, Primary: true},
			{Name: "Name", Type: "TEXT", NotNull: false, Primary: false},
		}
		assert.Equals(t, expected, actual)
	})
	t.Run("TestCorrectReplicaTableColumns", func(t *testing.T) {
		actual := scanColumns("child", t)
		expected := []tableInfo{
			{Name: "ID", Type: "INTEGER", NotNull: false, Primary: true},
			{Name: "Parent", Type: "INTEGER", NotNull: false, Primary: false},
			{Name: "Value", Type: "TEXT", NotNull: true, Primary: false},
		}
		assert.Equals(t, expected, actual)
	})
	t.Run("TestForeignKeyExists", func(t *testing.T) {
		actual := scanForeignKeys("child", t)
		expected := []foreignKeyInfo{
			{Table: "parent", From: "Parent", To: "ID"},
		}
		assert.Equals(t, expected, actual)
	})
}

type superior struct {
	ID        int64       `persistence:"primary_key,generated"`
	Inferiors []*Inferior `persistence:"many_to_many"`
}
type Superior struct {
	superior
}
type inferior struct {
	ID        int64       `persistence:"primary_key,generated"`
	Superiors []*Superior `persistence:"many_to_many"`
}

type Inferior struct {
	inferior
}

func TestRecursiveManyToManyJoins(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	assert.Nil(t, err)

	t.Run("TestCreateTables", func(t *testing.T) {
		err = c.BuildStatement().CreateFromModel(Superior{}, Inferior{})
		assert.Nil(t, err)
	})
	t.Run("TestSuperiorTableCreated", func(t *testing.T) {
		exists, err := c.BuildStatement().HasTable(Superior{})
		assert.Nil(t, err)
		assert.True(t, exists)
	})
	t.Run("TestInferiorTableCreated", func(t *testing.T) {
		exists, err := c.BuildStatement().HasTable(Inferior{})
		assert.Nil(t, err)
		assert.True(t, exists)
	})
	t.Run("TestManyToManyTableCreated", func(t *testing.T) {
		stmt := persistence.PrepareQuery(`SELECT COUNT(name) FROM sqlite_master WHERE type='table' AND name='Superior_Inferior';`)
		var exists bool
		err := c.RawStatement().QueryRow(stmt, &exists)
		assert.Nil(t, err)
		assert.True(t, exists)
	})
}

func TestInsertUpsert(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	assert.Nil(t, err)

	type user struct {
		ID    uint64 `persistence:"primary_key,generated"`
		Value string
	}
	t.Run("TestCreateTable", func(t *testing.T) {
		err := c.BuildStatement().CreateFromModel(user{})
		assert.Nil(t, err)
	})
	t.Run("TestInsert", func(t *testing.T) {
		given := user{
			Value: "example",
		}
		err := c.BuildStatement().Insert(&given)
		assert.Nil(t, err)
		assert.Equals(t, 1, given.ID)
	})
	t.Run("TestInsertFilledStruct", func(t *testing.T) {
		given := user{
			ID:    4,
			Value: "example",
		}
		err := c.BuildStatement().Insert(&given)
		assert.Nil(t, err)
		assert.Equals(t, 4, given.ID)
	})
	t.Run("TestUpsertNew", func(t *testing.T) {
		given := user{
			Value: "new",
		}
		err := c.BuildStatement().Upsert(&given)
		assert.Nil(t, err)
		assert.Equals(t, 5, given.ID)
	})
	t.Run("TestUpsertExisting", func(t *testing.T) {
		given := user{
			ID:    1,
			Value: "different",
		}
		err := c.BuildStatement().Upsert(&given)
		assert.Nil(t, err)
		assert.Equals(t, 1, given.ID)
	})
}

func TestGeneralQueries(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	assert.Nil(t, err)

	type user struct {
		ID    uint64 `persistence:"primary_key,generated"`
		Value string
	}

	t.Run("TestCreateTable", func(t *testing.T) {
		err := c.BuildStatement().CreateFromModel(user{})
		assert.Nil(t, err)
	})
	t.Run("TestInserts", func(t *testing.T) {
		given := []interface{}{
			user{Value: "one"},
			user{Value: "two"},
			user{Value: "three"},
			user{Value: "four"},
			user{Value: "five"},
			user{Value: "six"},
			user{Value: "seven"},
			user{Value: "eight"},
			user{Value: "nine"},
			user{Value: "ten"},
		}
		err := c.BuildStatement().Insert(given...)
		assert.Nil(t, err)
	})
	t.Run("TestPointerToNilReturnsError", func(t *testing.T) {
		var given *user
		err := c.BuildStatement().From(user{}).Query(given)
		assert.Equals(t, internal.ErrNilExportProvided, err)
	})
	t.Run("TestQuerySingleValue", func(t *testing.T) {
		var given user
		err := c.BuildStatement().From(user{}).Query(&given)
		assert.Nil(t, err)
		assert.NotEquals(t, 0, given.ID)
	})
	t.Run("TestQueryAllValues", func(t *testing.T) {
		var given []user
		err := c.BuildStatement().From(user{}).Query(&given)
		assert.Nil(t, err)
		assert.Equals(t, 10, len(given))
	})
	t.Run("TestQueryAllPtrValues", func(t *testing.T) {
		var given []*user
		err := c.BuildStatement().From(user{}).Query(&given)
		assert.Nil(t, err)
		assert.Equals(t, 10, len(given))
	})
	t.Run("TestQueryFilteredValues", func(t *testing.T) {
		var given []user
		err := c.BuildStatement().From(user{}).Where("id > 5").Query(&given)
		assert.Nil(t, err)
		assert.Equals(t, 5, len(given))
	})
	t.Run("TestQueryLimitedValues", func(t *testing.T) {
		var given []user
		var limit = 8
		err := c.BuildStatement().From(user{}).Limit(limit).Query(&given)
		assert.Nil(t, err)
		assert.Equals(t, limit, len(given))
	})
	t.Run("TestQueryOrderedValues", func(t *testing.T) {
		var given []user
		err := c.BuildStatement().From(user{}).Order("ID").Query(&given)
		assert.Nil(t, err)
		assert.Equals(t, 10, len(given))
		for i, provided := range given {
			assert.Equals(t, i+1, provided.ID)
		}
	})
}

func TestEndToEndBuilder(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	assert.Nil(t, err)
	type role struct {
		RoleID int64  `persistence:"primary,generated"`
		Name   string `persistence:"unique"`
	}
	type user struct {
		ID        int64  `persistence:"primary_key,generated"`
		Value     string `persistence:"combined_key:1,unique"`
		Value2    string `persistence:"combined_key:1,not_null"`
		Grp       bool   `persistence:"combined_key:2"`
		Grp2      bool   `persistence:"combined_key:2"`
		Ignore    []string
		Roles     []*role   `persistence:"many_to_many"`
		TsCreated time.Time `persistence:"generated,not_null"`
	}
	type userentry struct {
		UserID int64  `persistence:"primary_key,references:user(id)"`
		Value  string `persistence:"type:char(64)"`
	}
	t.Run("TestCreateTable", func(t *testing.T) {
		err := c.BuildStatement().CreateFromModel(user{}, role{}, userentry{})
		assert.Nil(t, err)
	})
	t.Run("TestHasTable", func(t *testing.T) {
		for _, face := range []interface{}{user{}, userentry{}, role{}} {
			exists, err := c.BuildStatement().HasTable(face)
			assert.Nil(t, err)
			assert.True(t, exists)
		}
		type invalid struct{}
		exists, err := c.BuildStatement().HasTable(invalid{})
		assert.Nil(t, err)
		assert.False(t, exists)
	})
	t.Run("TestHasTable_user_roles", func(t *testing.T) {
		stmt := persistence.PrepareQuery(`SELECT COUNT(name) FROM sqlite_master WHERE type='table' AND name= 'user_role';`)
		var exists bool
		err := c.RawStatement().QueryRow(stmt, &exists)
		assert.Nil(t, err)
		assert.True(t, exists)
	})
	t.Run("TestInsertUsers", func(t *testing.T) {
		users := []user{
			{Value: "roles", Value2: "value2", Grp: true},
			{Value: "roles2", Value2: "value2", Grp2: true},
			{Value: "roles3", Value2: "value2", Grp: true, Grp2: true},
		}
		err := c.BuildStatement().Insert(
			users[0],
			&users[1],
			&users[2],
		)
		assert.Nil(t, err)
		assert.Equals(t, 0, users[0].ID)
		assert.Equals(t, 2, users[1].ID)
		assert.Equals(t, 3, users[2].ID)
	})
	t.Run("TestQueryInsertedUsers", func(t *testing.T) {
		var users []user
		err := c.BuildStatement().From(user{}).Query(&users)
		assert.Nil(t, err)
		expected := []user{
			{ID: 1, Value: "roles", Value2: "value2", Grp: true},
			{ID: 2, Value: "roles2", Value2: "value2", Grp2: true},
			{ID: 3, Value: "roles3", Value2: "value2", Grp: true, Grp2: true},
		}
		assert.Equals(t, expected, users)
	})
	t.Run("TestCountRows", func(t *testing.T) {
		actual, err := c.BuildStatement().From(user{}).Where("id = ?", 1).Or("id = ?", 2).Count()
		assert.Nil(t, err)
		assert.Equals(t, 2, actual)
	})
	t.Run("TestInsertRoles", func(t *testing.T) {
		roles := []role{
			{Name: "parent"},
			{Name: "child"},
			{Name: "uncle"},
			{Name: "sibling"},
		}
		err := c.BuildStatement().Insert(
			&roles[0],
			&roles[1],
			&roles[2],
			&roles[3],
		)
		assert.Nil(t, err)
		assert.Equals(t, 1, roles[0].RoleID)
		assert.Equals(t, 2, roles[1].RoleID)
		assert.Equals(t, 3, roles[2].RoleID)
		assert.Equals(t, 4, roles[3].RoleID)
	})
	t.Run("TestQueryInsertedRoles", func(t *testing.T) {
		var roles []role
		err := c.BuildStatement().From(role{}).Query(&roles)
		assert.Nil(t, err)
		expected := []role{
			{RoleID: 1, Name: "parent"},
			{RoleID: 2, Name: "child"},
			{RoleID: 3, Name: "uncle"},
			{RoleID: 4, Name: "sibling"},
		}
		assert.Equals(t, expected, roles)
	})
	t.Run("TestConstrainedQueryUser", func(t *testing.T) {
		var actual user
		expected := user{ID: 1, Value: "roles", Value2: "value2", Grp: true}
		err := c.BuildStatement().From(user{}).Where("user.id = ?", 1).Order("id DESC").Query(&actual)
		assert.Nil(t, err)
		assert.Equals(t, expected, actual)

		expected = user{ID: 3, Value: "roles3", Value2: "value2", Grp: true, Grp2: true}
		err = c.BuildStatement().From(user{}).Where("user.id = ?", 3).Order("id DESC").Query(&actual)
		assert.Nil(t, err)
		assert.Equals(t, expected, actual)
	})
	t.Run("TestUpdateUser", func(t *testing.T) {
		err := c.BuildStatement().Update(
			user{ID: 1, Value: "Updated Value", Grp: true},
		)
		assert.Nil(t, err)
	})
	t.Run("TestDeleteUser", func(t *testing.T) {
		err := c.BuildStatement().Where("Grp2 = ?", true).Delete(
			user{},
		)
		assert.Nil(t, err)
	})
	t.Run("TestQueryDeletedUser", func(t *testing.T) {
		var actual []user
		expected := []user{
			{ID: 1, Value: "Updated Value", Grp: true},
		}
		err := c.BuildStatement().From(user{}).Where("id = ?", 1).Or("id = ?", 2).Order("id DESC").Query(&actual)
		assert.Nil(t, err)
		assert.Equals(t, expected, actual)
	})
	t.Run("TestQueryWithNoResults", func(t *testing.T) {
		var actual user
		expected := user{}

		err := c.BuildStatement().From(user{}).Where("id = ?", 1).And("id = ?", 2).Order("id DESC").Query(&actual)
		assert.Nil(t, err)
		assert.Equals(t, expected, actual)
	})
	t.Run("TestDropTable", func(t *testing.T) {
		err = c.BuildStatement().DropFromModel(userentry{}, role{}, user{})
		assert.Nil(t, err)
		for _, face := range []interface{}{user{}, userentry{}, role{}} {
			exists, err := c.BuildStatement().HasTable(face)
			assert.Nil(t, err)
			assert.False(t, exists)
		}
	})
}
