package e2e

import (
	"database/sql"
	"testing"

	"github.com/cosban/assert"
	_ "github.com/lib/pq"

	"gitlab.com/cosban/persistence"
)

func TestEndToEndRaw(t *testing.T) {
	c, err := persistence.Open("sqlite3", "file::memory:?mode=memory")
	assert.Nil(t, err)

	t.Run("TestCreateTable", func(t *testing.T) {
		_, err := c.RawStatement().PrepareAndExecute(`CREATE TABLE raw_example (id int, value text);`)
		assert.Nil(t, err)
	})
	t.Run("TestInsertRow", func(t *testing.T) {
		_, err := c.RawStatement().PrepareAndExecute(`INSERT INTO raw_example VALUES ($1, $2)`, 1, "givenText")
		assert.Nil(t, err)
	})
	t.Run("TestQueryRow", func(t *testing.T) {
		var actualText string

		query := persistence.Prepare("SELECT value FROM raw_example")
		err := c.RawStatement().QueryRow(query, &actualText)

		assert.Nil(t, err)
		assert.Equals(t, "givenText", actualText)
	})
	t.Run("TestRunningMultipleStatements", func(t *testing.T) {
		delete := persistence.Prepare("DELETE FROM raw_example")
		insert1 := persistence.Prepare("INSERT INTO raw_example VALUES($1, $2)", 2, "secondText")
		insert2 := persistence.Prepare("INSERT INTO raw_example VALUES($1, $2)", 3, "third text")
		err := c.RawStatement().BlindlyExecuteStatements(delete, insert1, insert2)
		assert.Nil(t, err)
	})
	t.Run("TestQueryMultipleRows", func(t *testing.T) {
		results := map[int]string{}
		var rows *sql.Rows
		rows, err := c.RawStatement().PrepareAndQuery("SELECT id, value FROM raw_example;")
		assert.Nil(t, err)
		defer rows.Close()
		for rows.Next() {
			var key int
			var value string
			err = rows.Scan(&key, &value)
			assert.Nil(t, err)
			results[key] = value
		}
		assert.Equals(t, 2, len(results))
		assert.Equals(t, "secondText", results[2])
		assert.Equals(t, "third text", results[3])
	})
	t.Run("TestUpdateRow", func(t *testing.T) {
		var newGivenText = "new example text"
		exec, err := c.RawStatement().PrepareAndExecute(`UPDATE raw_example SET value = ? WHERE id = ?;`, newGivenText, 3)
		assert.Nil(t, err)
		total, _ := exec.RowsAffected()
		assert.Equals(t, 1, total)

		results := map[int]string{}
		var rows *sql.Rows
		rows, err = c.RawStatement().PrepareAndQuery("SELECT id, value FROM raw_example;")
		assert.Nil(t, err)
		defer rows.Close()
		for rows.Next() {
			var key int
			var value string
			err = rows.Scan(&key, &value)
			assert.Nil(t, err)
			results[key] = value
		}
		assert.Equals(t, 2, len(results))
		assert.Equals(t, "secondText", results[2])
		assert.Equals(t, newGivenText, results[3])
	})
	t.Run("TestDropTable", func(t *testing.T) {
		_, err := c.RawStatement().PrepareAndExecute("DROP TABLE raw_example")
		assert.Nil(t, err)
	})
	t.Run("TestErrorThrownOnDroppingNonexistantTable", func(t *testing.T) {
		stmt := persistence.Prepare("DROP TABLE raw_example")
		err := c.RawStatement().BlindlyExecuteStatements(stmt, stmt)
		assert.Equals(t, `{DROP TABLE raw_example [] true} - no such table: raw_example`, err.Error())
	})
}
