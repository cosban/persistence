package execution

import "database/sql"

type Database interface {
	Executor
	Ping() error
	Begin() (Transaction, error)
	Close()
}

type db struct {
	db *sql.DB
}

func NewDatabase(d *sql.DB) Database {
	return &db{d}
}

func (d *db) Exec(query string, args ...interface{}) (sql.Result, error) {
	return d.db.Exec(query, args...)
}

func (d *db) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return d.db.Query(query, args...)
}

func (d *db) Ping() error {
	return d.db.Ping()
}

func (d *db) Begin() (Transaction, error) {
	dtx, err := d.db.Begin()
	if err != nil {
		return nil, err
	}
	tx := &tx{
		tx:     dtx,
		errors: []error{},
	}
	return tx, nil
}

func (d *db) Close() {
	d.db.Close()
}
