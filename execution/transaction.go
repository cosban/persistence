package execution

import (
	"database/sql"

	"gitlab.com/cosban/persistence/internal"
)

type Transaction interface {
	Executor
	Commit() error
	Rollback() error
	Execute() error
}

type tx struct {
	tx     *sql.Tx
	errors []error
}

// Commit commits the transaction
func (t *tx) Commit() error {
	return t.tx.Commit()
}

// Rollback rolls back the transaction
func (t *tx) Rollback() error {
	return t.tx.Rollback()
}

// Execute determines whether a commit or rollback needs to take place by checking whether any of the components which
// make up the transaction produced an error. A commit occurs on no error, otherwise a rollback takes place. The boolean
// value returned is an indicator of whether the transaction attempted to commit
func (t *tx) Execute() error {
	if len(t.errors) > 0 {
		err := t.Rollback()
		if err != nil {
			return err
		}
		return internal.ErrPriorTxIssue
	}
	return t.Commit()
}

func (t *tx) Exec(query string, args ...interface{}) (sql.Result, error) {
	r, err := t.tx.Exec(query, args...)
	return r, t.err(err)
}

// Exec executes a statement
func (t *tx) Query(query string, args ...interface{}) (*sql.Rows, error) {
	r, err := t.tx.Query(query, args...)
	return r, t.err(err)
}

func (t *tx) err(e error) error {
	if e != nil {
		t.errors = append(t.errors, e)
	}
	return e
}
