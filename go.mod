module gitlab.com/cosban/persistence

go 1.14

require (
	github.com/cosban/assert v1.1.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0
)
