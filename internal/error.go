package internal

import (
	"errors"
	"fmt"

	"gitlab.com/cosban/persistence/statement"
)

var (
	// ErrNoRows is returned when a query results in no viable rows
	ErrNoRows = errors.New("Query returned no rows")
	// ErrNoTxStarted is returned when an attempt to use a transaction takes place even though no transaction has
	// started
	ErrNoTxStarted = errors.New("No transactions in progress")
	// ErrPriorTxIssue is returned when there is an issue with an execution within a transaction but the framework is
	// unable to figure out what the issue was
	ErrPriorTxIssue = errors.New("Transaction was rolled back due to issues with underlying transactions")
	// ErrInvalidOutInterface is returned when a query is performed but the given type to fill is not a pointer to slice
	// or struct
	ErrInvalidOutInterface = errors.New("out must be a pointer to a slice or struct")
	// ErrNilExportProvided is returned when the underlying value of the interface to be populated from a query is nil
	ErrNilExportProvided = errors.New("Nil export provided")
)

// TransactionError occurs whenever a statement sent to the sql server has a problem
type TransactionError struct {
	err   error
	cause statement.Statement
}

// Error is used to create a new TransactionError. If the underlying error is nil, then nil is returned.
func Error(err error, cause statement.Statement) error {
	if err == nil {
		return nil
	}
	return &TransactionError{
		err:   err,
		cause: cause,
	}
}

// Error implements the error interface for TransactionError
func (e *TransactionError) Error() string {
	return fmt.Sprintf("%v - %s", e.cause, e.err.Error())
}
