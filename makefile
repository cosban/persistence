PROJECT := persistence
PKG := "gitlab.com/cosban/$(PROJECT)"

.PHONY: all build clean test race coverage

all: build

test: clean
	@GO111MODULE=on go test ./... -short -coverprofile coverage.log
	@GO111MODULE=on go test $(PKG)/e2e -coverpkg $(PKG) -coverprofile coverage.e2e.log
	@tail -n +2 coverage.e2e.log >> coverage.log
	@go tool cover -func=coverage.log

race:
	@GO111MODULE=on go test -v -race -short ./...

build:
	@go build -i -v $(PKG)

clean:
	@rm -f $(PROJECT)
	@rm -f coverage*.log
