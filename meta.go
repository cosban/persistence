package persistence

import (
	"reflect"
	"strings"
)

// Meta is a representation of a struct or value which has a name and a type
type Meta interface {
	GetName() string
	GetType() reflect.Type
}

// TableMeta contains all of the information needed to construct a new table
type TableMeta struct {
	Name            string
	Pointer         bool
	JoinTable       bool
	Type            reflect.Type
	Schema          string
	Columns         *FieldMetas
	Primary         *FieldMetas
	Joins           map[string][]TableMeta
	ManyToManyJoins map[string][]TableMeta
	Zero            bool
}

// IsEmpty returns true if a tableMeta does not represent anything
func (meta TableMeta) IsEmpty() bool {
	return reflect.DeepEqual(reflect.Zero(reflect.TypeOf(meta)).Interface(), meta)
}

// GetName returns the name of the item represented by the TableMeta
func (meta TableMeta) GetName() string {
	return meta.Name
}

// GetType returns the type of data that is represented by the TableMeta
func (meta TableMeta) GetType() reflect.Type {
	return meta.Type
}

// GetRowIDField returns a field which should represent the rowid for any row within a table. This function is not
// supported by every dialect of sql.
func (meta TableMeta) GetRowIDField() FieldMeta {
	columns := meta.Primary.Iter()
	for columns.Next() {
		_, field := columns.Current()
		if field.Tag.Generated && field.Tag.PrimaryKey {
			return field
		}
	}
	return FieldMeta{}
}

// Copy returns an equivalent copy of the receiver. This copy is able to be modified without changing the original.
func (meta TableMeta) Copy() TableMeta {
	c := TableMeta{
		Name:            meta.Name,
		Pointer:         meta.Pointer,
		Schema:          meta.Schema,
		Type:            meta.Type,
		Columns:         meta.Columns.Copy(),
		Primary:         meta.Primary.Copy(),
		Joins:           map[string][]TableMeta{},
		ManyToManyJoins: map[string][]TableMeta{},
	}
	copyJoins(meta.Joins, &c.Joins)
	copyJoins(meta.ManyToManyJoins, &c.ManyToManyJoins)
	return c
}

func copyJoins(joins map[string][]TableMeta, dest *map[string][]TableMeta) {
	for k, v := range joins {
		metas := make([]TableMeta, len(v))
		copy(metas, v)
		(*dest)[k] = metas
	}
}

// Export takes in an interface, and if that interface a pointer to the type that the TableMeta represents, then the
// TableMeta will copy its values into the interface.
func (meta TableMeta) Export(i interface{}, exports ...Export) {
	in := reflect.ValueOf(i)
	if in.Kind() == reflect.Ptr && in.Elem().Type() == meta.Type {
		for i := 0; i < meta.Columns.Size(); i++ {
			name, column := meta.Columns.Index(i)
			field := in.Elem().FieldByName(name)
			value := reflect.ValueOf(column.Value())
			field.Set(value)
		}
		for _, export := range exports {
			appendJoinItem(meta.Joins, export, in)
			appendJoinItem(meta.ManyToManyJoins, export, in)
		}
	}
}

func appendJoinItem(joins map[string][]TableMeta, export Export, in reflect.Value) {
	if metas, ok := joins[export.Name]; ok {
		for _, modelMeta := range metas {
			model := reflect.New(export.Meta.Type).Interface()
			modelMeta.Export(model)
			field := in.Elem().FieldByName(export.Name)
			value := reflect.ValueOf(model)
			if !modelMeta.Pointer {
				value = value.Elem()
			}
			field.Set(reflect.Append(field, value))
		}
	}
}

// AreAllColumnsPrimary returns true if every column within a table is also a primary key
func (meta TableMeta) AreAllColumnsPrimary() bool {
	return meta.Columns.Size() == meta.Primary.Size()
}

// ToMap returns a map representation of a TableMeta where the key is the name of each FieldMeta and the value is the
// value that each FieldMeta represents
func (meta TableMeta) ToMap() map[string]interface{} {
	result := map[string]interface{}{}
	for i := 0; i < meta.Columns.Size(); i++ {
		name, column := meta.Columns.Index(i)
		result[name] = column.Value()
	}
	return result
}

func fillTableMeta(
	out *TableMeta,
	names []string,
	columns []interface{},
) {
	for i, column := range columns {
		field := out.Columns.Get(names[i])
		if field.Valid {
			(&field).SetValue(reflect.ValueOf(column).Elem().Interface())
			out.Columns.Set(field.Name, field)
		}
	}
}

func fillJoinTableMetas(
	out *TableMeta,
	names []string,
	columns []interface{},
	joins []JoinConstraint,
) {
	for _, join := range joins {
		fillJoin(&out.Joins, join.Export, names, columns)
		fillJoin(&out.ManyToManyJoins, join.Export, names, columns)
	}
}

func fillJoin(join *map[string][]TableMeta, export Export, names []string, columns []interface{}) {
	if _, ok := (*join)[export.Name]; !ok {
		return
	}
	joiner := export.Meta.Copy()
	fillTableMeta(&joiner, names, columns)
	if len((*join)[export.Name]) == 1 && (*join)[export.Name][0].Zero {
		(*join)[export.Name] = []TableMeta{}
	}
	(*join)[export.Name] = append((*join)[export.Name], joiner)
}

// FieldMeta provides data used to determine the name, type, and constraints belonging to a column
type FieldMeta struct {
	Name       string
	Type       reflect.Type
	DataType   string
	Tag        Tag
	Valid      bool
	References *reference
	value      interface{}
	zero       bool
	set        bool
}

type reference struct {
	Table TableMeta
	Field FieldMeta
}

func (m *FieldMeta) PopulateForeignKeyReferences(registeredMetas []*TableMeta) {
	if m.Tag.References == "" {
		return
	}
	for _, meta := range registeredMetas {
		if m.Tag.References == meta.GetName() {
			m.References = &reference{
				Table: *meta,
				Field: meta.GetRowIDField(),
			}
			return
		}
	}
}

// SetValue sets the underlying value of a FieldMeta
func (m *FieldMeta) SetValue(i interface{}) {
	m.value = i
	t := reflect.TypeOf(i)
	m.zero = reflect.DeepEqual(i, reflect.Zero(t).Interface())
	m.set = m.set || t.Kind() == reflect.Bool || !m.zero
}

// Value returns the underlying value of a FieldMeta
func (m FieldMeta) Value() interface{} {
	return m.value
}

// Zero returns true if the FieldMeta does not represent a value that has been set
func (m FieldMeta) Zero() bool {
	if m.set {
		return m.zero
	}
	return true
}

// Set returns true if the underlying value of the Fieldmeta has been set
func (m FieldMeta) Set() bool {
	return m.set
}

// GetName returns the column name of a field. Tags may overwrite the default, which is the name of the variable.
func (m FieldMeta) GetName() string {
	if !m.Tag.Valid || m.Tag.Name == "" {
		return m.Name
	}
	return m.Tag.Name
}

// GetType returns the underlying type that a FieldMeta represents
func (m FieldMeta) GetType() reflect.Type {
	return m.Type
}

// FieldMetas is an ordered map of FieldMeta keyed by each FieldMeta's name as provided by the the item containing it
type FieldMetas struct {
	i       int
	ordered []string
	set     map[string]FieldMeta
}

// NewFieldMetasFrom takes in FieldMetas and returns a pre-populated *FieldMetas
func NewFieldMetasFrom(metas ...FieldMeta) *FieldMetas {
	meta := NewFieldMetas()
	for _, field := range metas {
		meta.Set(field.Name, field)
	}
	return meta
}

// NewFieldMetas returns a blank *FieldMetas
func NewFieldMetas() *FieldMetas {
	return &FieldMetas{
		ordered: []string{},
		set:     map[string]FieldMeta{},
	}
}

// Copy returns a copy of the FieldMetas which can be used without affecting the original
func (m *FieldMetas) Copy() *FieldMetas {
	other := NewFieldMetas()
	for i := 0; i < m.Size(); i++ {
		k, v := m.Index(i)
		other.Set(k, v)
	}
	return other
}

// Set puts the key and value into the map ordered map. If the key already exists, its order is maintained.
func (m *FieldMetas) Set(key string, value FieldMeta) {
	if v := m.Get(key); !v.Valid {
		m.ordered = append(m.ordered, key)
	}
	m.set[key] = value
}

// Size returns the number of items that are contained within the FieldMetas
func (m *FieldMetas) Size() int {
	return len(m.ordered)
}

// Index returns the key and value stored at the requested index
func (m *FieldMetas) Index(i int) (string, FieldMeta) {
	key := m.ordered[i]
	return key, m.set[key]
}

// Iter returns a copy of the FieldMetas that is able to be cleanly iterated over
func (m *FieldMetas) Iter() *FieldMetas {
	c := m.Copy()
	c.i = -1
	return c
}

// Next moves moves the internal index forward and returns true if there is a value available to be read
func (m *FieldMetas) Next() bool {
	m.i++
	if m.i >= m.Size() || m.Size() == 0 {
		return false
	}
	return true
}

// Current returns the key and value stored at the position of the internal index
func (m *FieldMetas) Current() (string, FieldMeta) {
	return m.Index(m.i)
}

// Get returns the FieldMeta which corresponds to the given key
func (m *FieldMetas) Get(key string) FieldMeta {
	if v, ok := m.set[key]; ok {
		return v
	}
	for _, k := range m.ordered {
		lower := strings.ToLower(k)
		if lower == key {
			v, _ := m.set[k]
			m.set[lower] = v
			return v
		}
	}
	return FieldMeta{}
}

func (m *FieldMetas) PopulateForeignKeyReferences(registeredMetas []*TableMeta, template FieldMeta) {
	for _, v := range m.set {
		(&v).PopulateForeignKeyReferences(registeredMetas)
	}
}

// Tag is the representation of a persistence struct tag. If no tags are provided, default values are used. When a tag,
// such as the `name` tag requires a value, the tag and its value are separated by a colon ':'. The following is an
// example of a struct which makes use of the persistence tags.
//
// 		type PersistentStruct struct {
//			ID uint32 `persistence:"primary,name:persistentID,generated"` // tags are comma separated
//			Value string `persistence:"not_null"`
//		}
type Tag struct {
	// Valid is true if the tag is able to be parsed. Users are unable to modify this value.
	Valid bool
	// Name is set via `name` and requires a value. Represents the name of the column. default: Field name
	Name string
	// NotNullable is set to true using `not_null` and does not use additional values. Represents whether a column is
	// nullable. default: false
	NotNullable bool
	// Unique is set to true using `unique` and does not use additional values. Represents whether a column is unique.
	// default: false
	Unique bool
	// PrimaryKey is set to true using either `primary`, or `primary_key` and does not use additional values. Represents
	// whether a column is to be used as a primary key for a table. Zero, One, or Multiple primary keys may be present.
	// default: false
	PrimaryKey bool
	// CombinedKey is set via `combined`, or `combined_key` and requires a value which represents the name of the
	// combined key. This is used to create an additional index which is not a primary key. Multiple fields may use the
	// same name and they will be combined into the key. Multiple combined keys may be present in a struct. default: ""
	CombinedKey string
	// DefaultValue is set via `default` and requires a value which represents the default value of the column if none
	// is provided. This value will retain its sql type information, and therefore values such as `now()` may be used.
	// default: ""
	DefaultValue string
	// References is set via `references`, `foreign_key`, or `foreign` and requires a value which represents the table
	// and column to be referenced in a foreign key constraint on this column. Right now this format requires making use
	// of the exact reference of the table and column which the dialect uses (i.e. `table(column)` in postgres), but
	// this is subject to change in a later release. default: ""
	References string
	// Type is set via `type` and does not use additional values. This is used to specify a non-default type for a
	// column in the event that the dialect driver would otherwise provide one that is not desired for this field.
	// default: ""
	Type string
	// Join is set to true via `join` and does not use additional values. This should only be used on slices and maps in
	// order to notify persistence that items in the slice or map are expected to be persisted in a one-to-one, or
	// one-to-many join table. default: false
	Join bool
	// ManyToManyJoin is set to true via `many_to_many` and does not use additional values. This should only be used on
	// slices and maps in order to notify persistence that items in the slice or map are expected to be persisted in a
	// many-to-many fashion using a join table. default: false
	ManyToManyJoin bool
	// Generated is set to true via `generated` and does not use additional values. Represents whether the value of a
	// column is to be created by the database instead of it being provided. Drivers should specify how columns are
	// generated for any given generated column and not each sql type supports generation. default: false
	Generated bool
}
