package persistence

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
)

var (
	// UintPtrType is a reflect.Type representing a *uint
	UintPtrType = reflect.TypeOf((*uint)(nil))
	// UintType is a reflect.Type representing a uint
	UintType = UintPtrType.Elem()

	// StringPtrType is a reflect.Type representing a *string
	StringPtrType = reflect.TypeOf((*string)(nil))
	// StringType is a reflect.Type representing a string
	StringType = StringPtrType.Elem()
)

type metaModel struct {
	ID      uint   `persistence:"primary,type:serial"`
	Column  string `persistence:"name:field"`
	Slice   []int
	Joiners []metaJoiner `persistence:"join"`
	Deleted *time.Time   `persistence:"name:deleted"`
}

type metaJoiner struct {
	ID    uint `persistence:"primary,type:serial"`
	Field bool
}

func TestToMap(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
	}
	meta := ObtainTableMeta(given, mockValidator)

	actual := meta.ToMap()

	expected := map[string]interface{}{
		"ID":      uint(23),
		"Column":  "test",
		"Deleted": (*time.Time)(nil),
	}

	assert.Equals(expected, actual)
}

func TestGetNameDefault(t *testing.T) {
	given := FieldMeta{Name: "ID", Type: UintType, Tag: Tag{Valid: true, Type: "serial"}}
	testGetName(t, given, "ID")
}

func TestGetNameOverwritten(t *testing.T) {
	given := FieldMeta{Name: "Column", Type: StringType, Tag: Tag{Valid: true, Name: "field"}}
	testGetName(t, given, "field")
}

func testGetName(t *testing.T, given FieldMeta, expected string) {
	t.Helper()
	assert := assert.New(t)
	actual := given.GetName()
	assert.Equals(expected, actual)
}

func TestCopy(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
		Joiners: []metaJoiner{
			{
				ID:    8,
				Field: false,
			},
		},
	}

	meta := ObtainTableMeta(given, mockValidator)

	copiedMeta := meta.Copy()
	assert.Equals(meta, copiedMeta)

	// modifying one meta should not modify the other
	meta.Joins["Joiners"] = nil
	assert.NotEquals(meta, copiedMeta)
}

func TestExport(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
	}

	meta := ObtainTableMeta(given, mockValidator)

	actual := metaModel{}

	expected := metaModel{
		ID:     23,
		Column: "test",
	}

	meta.Export(&actual)
	assert.Equals(expected, actual)
}

func TestExportBadInput(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
	}

	meta := ObtainTableMeta(given, mockValidator)

	actual, expected := model{}, model{}
	meta.Export(actual)
	assert.Equals(expected, actual)
}

func TestExportNoPointer(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
		Joiners: []metaJoiner{
			{
				ID:    8,
				Field: true,
			},
		},
	}
	meta := ObtainTableMeta(given, mockValidator)
	actual, expected := metaModel{}, metaModel{}
	meta.Export(actual)
	assert.Equals(expected, actual)
}

func TestExportWithJoins(t *testing.T) {
	assert := assert.New(t)

	given := metaModel{
		ID:      23,
		Column:  "test",
		Slice:   []int{1, 2, 3},
		Joiners: []metaJoiner{{ID: 8, Field: true}},
	}

	meta := ObtainTableMeta(&given, mockValidator)
	joinMeta := ObtainTableMeta(metaJoiner{}, mockValidator)

	actual := metaModel{}
	export := Export{
		Name: "Joiners",
		Meta: joinMeta,
	}
	meta.Export(&actual, export)

	expected := metaModel{
		ID:      23,
		Column:  "test",
		Joiners: []metaJoiner{{ID: 8, Field: true}},
	}

	assert.Equals(expected, actual)
}

func TestExportWithPointerJoins(t *testing.T) {
	type joiner struct {
		ID int
	}
	type model struct {
		ID      int
		Joiners []*joiner `persistence:"join"`
	}

	given := model{
		ID:      23,
		Joiners: []*joiner{&joiner{ID: 8}},
	}

	meta := ObtainTableMeta(&given, mockValidator)
	joinMeta := ObtainTableMeta(joiner{}, mockValidator)

	actual := model{}
	export := Export{
		Name: "Joiners",
		Meta: joinMeta,
	}
	meta.Export(&actual, export)

	expected := model{
		ID:      23,
		Joiners: []*joiner{&joiner{ID: 8}},
	}

	assert.Equals(t, expected, actual)
}

type universe struct {
	Name   string
	Cities []city `persistence:"join"`
}

type city struct {
	Population uint64
	Name       string
	Area       float64
	Costal     bool
}

func TestFillTableMeta(t *testing.T) {
	assert := assert.New(t)

	givenUint64 := uint64(8168564)
	givenString := "Gotham City"
	givenFloat64 := float64(846.9)
	givenBool := true

	givenColumns := []interface{}{&givenUint64, &givenString, &givenFloat64, &givenBool}

	given := ObtainTableMeta(city{}, mockValidator)

	fillTableMeta(&given, []string{"Population", "Name", "Area", "Costal"}, givenColumns)

	actual := city{}
	given.Export(&actual)

	expected := city{Population: 8168564, Name: "Gotham City", Costal: true}
	assert.Equals(expected, actual)
}

func TestFillJoinTableMeta(t *testing.T) {
	assert := assert.New(t)

	gothamPopulation := uint64(8168564)
	gothamName := "Gotham City"
	gothamArea := float64(846.9)
	gothamBool := true

	smallvillePopulation := uint64(110000)
	smallvilleName := "Smallville"
	smallvilleArea := float64(324.7)
	smallvilleBool := false

	gothamColumns := []interface{}{&gothamPopulation, &gothamName, &gothamArea, &gothamBool}
	smallvilleColumns := []interface{}{&smallvillePopulation, &smallvilleName, &smallvilleArea, &smallvilleBool}

	actual := ObtainTableMeta(universe{}, mockValidator)

	cityMeta := ObtainTableMeta(city{}, mockValidator)

	joins := []JoinConstraint{{Export: Export{Name: "Cities", Meta: cityMeta}}}

	fillJoinTableMetas(&actual, []string{"Population", "Name", "Area", "Costal"}, gothamColumns, joins)
	fillJoinTableMetas(&actual, []string{"Population", "Name", "Area", "Costal"}, smallvilleColumns, joins)

	expected := TableMeta{
		Name:   "universe",
		Type:   reflect.TypeOf(universe{}),
		Schema: "persist",
		Columns: NewFieldMetasFrom(
			FieldMeta{Name: "Name", Type: reflect.TypeOf(gothamName), value: "", set: false, zero: true, Tag: Tag{Valid: true}, Valid: true}),
		Primary: NewFieldMetas(),
		Joins: map[string][]TableMeta{
			"Cities": []TableMeta{
				{
					Name:   "city",
					Type:   reflect.TypeOf(city{}),
					Schema: "persist",
					Columns: NewFieldMetasFrom(
						FieldMeta{Name: "Population", Type: reflect.TypeOf(gothamPopulation), value: gothamPopulation, set: true, zero: false, Tag: Tag{Valid: true}, Valid: true},
						FieldMeta{Name: "Name", Type: reflect.TypeOf(gothamName), value: gothamName, set: true, zero: false, Tag: Tag{Valid: true}, Valid: true},
						FieldMeta{Name: "Costal", Type: reflect.TypeOf(gothamBool), value: gothamBool, set: true, zero: false, Tag: Tag{Valid: true}, Valid: true},
					),
					Primary:         NewFieldMetas(),
					Joins:           map[string][]TableMeta{},
					ManyToManyJoins: map[string][]TableMeta{},
				},
				TableMeta{
					Name:   "city",
					Type:   reflect.TypeOf(city{}),
					Schema: "persist",
					Columns: NewFieldMetasFrom(
						FieldMeta{Name: "Population", Type: reflect.TypeOf(smallvillePopulation), value: smallvillePopulation, set: true, zero: false, Tag: Tag{Valid: true}, Valid: true},
						FieldMeta{Name: "Name", Type: reflect.TypeOf(smallvilleName), value: smallvilleName, set: true, zero: false, Tag: Tag{Valid: true}, Valid: true},
						FieldMeta{Name: "Costal", Type: reflect.TypeOf(smallvilleBool), value: smallvilleBool, set: true, zero: true, Tag: Tag{Valid: true}, Valid: true},
					),
					Primary:         NewFieldMetas(),
					Joins:           map[string][]TableMeta{},
					ManyToManyJoins: map[string][]TableMeta{},
				},
			},
		},
		ManyToManyJoins: map[string][]TableMeta{},
	}
	assert.Equals(expected.Joins, actual.Joins)
	assert.Equals(expected.ManyToManyJoins, actual.ManyToManyJoins)
	assert.Equals(expected, actual)
}

func TestFieldMetasFromSameFieldMetasAreEquivalent(t *testing.T) {
	assert := assert.New(t)

	given := []FieldMeta{
		{Name: "StringField", Type: reflect.TypeOf(""), value: "value", DataType: "STRING", Tag: Tag{}, Valid: true},
		{Name: "Uint64Field", Type: reflect.TypeOf(uint64(8575)), value: 8575, DataType: "INTEGER", Tag: Tag{}, Valid: true},
		{Name: "BoolField", Type: reflect.TypeOf(true), value: true, DataType: "BOOLEAN", Tag: Tag{}, Valid: true},
		{Name: "TimeField", Type: reflect.TypeOf(time.Time{}), value: time.Now(), DataType: "DATETIME", Tag: Tag{}, Valid: true},
	}

	previous := NewFieldMetasFrom(given...)
	for i := 0; i < 100; i++ {
		next := NewFieldMetasFrom(given...)
		assert.Equals(previous, next)
		previous = next
	}
}

func TestSetValueNotCalled(t *testing.T) {
	meta := &FieldMeta{}

	assert.Nil(t, meta.Value())
	assert.True(t, meta.Zero())
	assert.False(t, meta.Set())
}

func TestSetValueNonZero(t *testing.T) {
	meta := &FieldMeta{}
	var given = 12
	meta.SetValue(given)

	assert.Equals(t, meta.Value(), given)
	assert.False(t, meta.Zero())
	assert.True(t, meta.Set())
}

func TestSetValueZero(t *testing.T) {
	meta := &FieldMeta{}
	var given = 0
	meta.SetValue(given)

	assert.Equals(t, meta.Value(), given)
	assert.True(t, meta.Zero())
	assert.False(t, meta.Set())
}

func TestSetValueFalse(t *testing.T) {
	meta := &FieldMeta{}
	var given = false
	meta.SetValue(given)

	assert.Equals(t, meta.Value(), given)
	assert.True(t, meta.Zero())
	assert.True(t, meta.Set())
}
