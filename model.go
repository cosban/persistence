package persistence

import (
	"fmt"
	"reflect"
	"sort"
	"strings"
)

// TypeValidator is used to determine whether a provided type is supported by the database
type TypeValidator interface {
	IsValidType(reflect.Type) bool
	GetSchema() string
}

var delimiter = ","

// SetDelimiter globally sets the delimiter that should be used to separate tags. Only use this if you definitely know
// what you are doing.
func SetDelimiter(replacement string) {
	delimiter = replacement
}

// DeriveTableMetaFromExport attempts to ObtainTableMeta with the provided export, it will search for the first
// underlying type of any slice/pointer/etc that is provided to it.
func DeriveTableMetaFromExport(export interface{}, validator TypeValidator) TableMeta {
	return ObtainTableMeta(export, validator)
}

// ObtainTableMeta constructs a TableMeta that represents the given interface. It also includes additional information
// that is necessary for writing/modifying/deleting the data in the database.
func ObtainTableMeta(model interface{}, validator TypeValidator) TableMeta {
	return obtainTableMeta(model, validator)
}

func obtainTableMeta(model interface{}, validator TypeValidator, registeredMetas ...*TableMeta) TableMeta {
	t := GetUsableType(model)
	v := GetUsableValue(model)

	meta := &TableMeta{
		Name:    t.Name(),
		Pointer: reflect.TypeOf(model).Kind() == reflect.Ptr,
		Type:    t,
		Schema:  validator.GetSchema(),
	}
	if validator != nil {
		meta.Columns = constructFields(t, v, validator)
		meta.Primary = getPrimary(meta.Columns)
	}
	registeredMetas = append(registeredMetas, meta)
	meta.Joins = findJoins(t, v, validator, func(t Tag) bool { return t.Join }, registeredMetas)
	meta.ManyToManyJoins = findJoins(t, v, validator, func(t Tag) bool { return t.ManyToManyJoin }, registeredMetas)
	return *meta
}

func obtainMultiJoinTableMeta(first, second TableMeta, schema string) TableMeta {
	name := getMultiJoinTableName(first, second)
	columns := obtainMultiJoinColumns(name, first, second)
	meta := TableMeta{
		Name:      name,
		JoinTable: true,
		Schema:    schema,
		Columns:   columns,
		Primary:   columns,
	}
	return meta
}

func obtainMultiJoinColumns(name string, metas ...TableMeta) *FieldMetas {
	columns := NewFieldMetas()
	for _, meta := range metas {
		primary := meta.Primary.Iter()
		for primary.Next() {
			key, column := meta.Primary.Current()
			column.Tag.Generated = false
			column.Tag.CombinedKey = fmt.Sprintf("%s_unique", name)
			column.Tag.References = fmt.Sprintf("%s(%s)", meta.GetName(), column.GetName())
			if meta.Schema != "" {
				column.Tag.References = fmt.Sprintf("%s.%s", meta.Schema, column.Tag.References)
			}
			(&column).SetValue(meta.Columns.Get(key).Value())
			name := fmt.Sprintf("%s_%s", meta.GetName(), key)
			column.Name = name
			columns.Set(name, column)
		}
	}
	return columns
}

func obtainTableMetaFromSliceField(meta TableMeta, fieldName string, validator TypeValidator) (TableMeta, bool) {
	field, exists := meta.Type.FieldByName(fieldName)
	if exists && field.Type.Kind() == reflect.Slice {
		elem := reflect.New(field.Type.Elem()).Interface()
		return ObtainTableMeta(elem, validator), true
	}
	return TableMeta{}, false
}

func getMultiJoinTableName(first, second TableMeta) string {
	return fmt.Sprintf("%s_%s", first.Name, second.Name)
}

func findJoins(t reflect.Type, v reflect.Value, validator TypeValidator, shouldUseJoin func(t Tag) bool, registeredMetas []*TableMeta) map[string][]TableMeta {
	joins := map[string][]TableMeta{}
	if validator == nil {
		return joins
	}
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		fv := v.Field(i)

		tag := parseJoinTag(f, fv)
		if f.Type.Kind() == reflect.Struct && f.Anonymous {
			anonJoins := findJoins(f.Type, fv, validator, shouldUseJoin, registeredMetas)
			for k, v := range anonJoins {
				joins[k] = v
			}
		}
		if shouldUseJoin(tag) && requiresJoin(f.Type) {
			if fv.Len() > 0 {
				for j := 0; j < fv.Len(); j++ {
					value := fv.Index(j).Interface()
					joiner := obtainTableMeta(value, validator, registeredMetas...)
					joins[f.Name] = append(joins[f.Name], joiner)
				}
			} else {
				underlying := f.Type
				for reflect.Ptr == underlying.Kind() || reflect.Slice == underlying.Kind() {
					underlying = underlying.Elem()
				}
				if !typeWasProcessed(underlying, registeredMetas) {
					joiner := obtainTableMeta(reflect.Zero(underlying).Interface(), validator, registeredMetas...)
					joiner.Zero = true
					joins[f.Name] = []TableMeta{joiner}
				} else {
					joins[f.Name] = []TableMeta{}
				}
			}
		}
	}
	return joins
}

func typeWasProcessed(t reflect.Type, registeredMetas []*TableMeta) bool {
	for _, meta := range registeredMetas {
		if t == meta.Type {
			return true
		}
	}
	return false
}

func constructFields(t reflect.Type, v reflect.Value, validator TypeValidator) *FieldMetas {
	switch t.Kind() {
	case reflect.Struct:
		return constructStructFields(t, v, validator)
	case reflect.Map:
		return constructMapFields(t, v, validator)
	}
	return NewFieldMetas()
}

func constructStructFields(t reflect.Type, v reflect.Value, validator TypeValidator) *FieldMetas {
	columns := NewFieldMetas()
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		fv := v.Field(i)
		if f.Type.Kind() == reflect.Struct && f.Anonymous {
			columns = constructFields(f.Type, fv, validator)
		}
		tag := parseTag(f, fv)
		if tag.Valid && validator.IsValidType(f.Type) && fv.CanInterface() {
			meta := FieldMeta{
				Name:  f.Name,
				Type:  f.Type,
				Tag:   tag,
				Valid: true,
			}
			(&meta).SetValue(fv.Interface())
			columns.Set(meta.Name, meta)
		}
	}
	return columns
}

func constructMapFields(t reflect.Type, v reflect.Value, validator TypeValidator) *FieldMetas {
	columns := NewFieldMetas()
	keys := []string{}
	for _, k := range v.MapKeys() {
		if k.Type().Kind() != reflect.String {
			continue
		}
		keys = append(keys, k.String())

	}
	sort.Strings(keys)
	for _, key := range keys {
		f := v.MapIndex(reflect.ValueOf(key))
		if f.Kind() == reflect.Interface {
			f = f.Elem()
		}
		// no tag... what to do... Assume no primary keys?
		if validator.IsValidType(f.Type()) {
			column := FieldMeta{
				Name:  key,
				Type:  f.Type(),
				Tag:   Tag{Valid: true},
				Valid: true,
			}
			(&column).SetValue(f.Interface())
			columns.Set(key, column)
		}
	}
	return columns
}

func getPrimary(columns *FieldMetas) *FieldMetas {
	primary := NewFieldMetas()
	cols := columns.Iter()
	for cols.Next() {
		name, column := columns.Current()
		if column.Tag.PrimaryKey {
			primary.Set(name, column)
		}
	}
	return primary
}

func requiresJoin(t reflect.Type) bool {
	return t.Kind() == reflect.Slice || t.Kind() == reflect.Map || t.Kind() == reflect.Array
}

func parseTag(f reflect.StructField, fv reflect.Value) Tag {
	label := f.Tag.Get("persistence")
	kind := fv.Kind()

	if label == "-" || kind == reflect.Slice || kind == reflect.Struct {
		return Tag{Valid: false}
	}
	return populateTagFields(label)
}

func parseJoinTag(f reflect.StructField, fv reflect.Value) Tag {
	label := f.Tag.Get("persistence")
	kind := fv.Kind()

	if label == "-" || kind != reflect.Slice {
		return Tag{Valid: false}
	}
	return populateTagFields(label)
}

func populateTagFields(label string) Tag {
	tag := Tag{Valid: true}
	fields := strings.Split(strings.TrimSpace(label), delimiter)
	for _, field := range fields {
		pair := strings.Split(strings.TrimSpace(field), ":")
		switch pair[0] {
		case "name":
			tag.Name = pair[1]
		case "primary_key", "primary":
			tag.PrimaryKey = true
		case "combined_key", "combined":
			tag.CombinedKey = pair[1]
		case "unique":
			tag.Unique = true
		case "not_null":
			tag.NotNullable = true
		case "default":
			tag.DefaultValue = pair[1]
		case "references", "foreign_key", "foreign":
			tag.References = pair[1]
		case "type":
			tag.Type = pair[1]
		case "generated":
			tag.Generated = true
		case "join":
			tag.Join = true
		case "many_to_many":
			tag.ManyToManyJoin = true
		default:
			if pair[0] != "" {
				err := fmt.Errorf("invalid tag found: %s", pair[0])
				panic(err)
			}
		}
	}
	return tag
}

func findField(i interface{}, name string, t reflect.Type) reflect.Value {
	value := GetUsableValue(i)
	field := value.FieldByName(name)
	if reflect.SliceOf(t) == field.Type() {
		return field
	}
	return reflect.Value{}
}
