package persistence

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
)

type model struct {
	ID      uint   `persistence:"primary,type:serial"`
	Column  string `persistence:"name:field"`
	Slice   []int
	Joiners []joiner   `persistence:"many_to_many"`
	Deleted *time.Time `persistence:"name:deleted"`
}

type joiner struct {
	ID     uint    `persistence:"primary,type:serial"`
	models []model `persistence:"many_to_many"`
	Field  bool
}

func TestObtainTableMeta(t *testing.T) {
	assert := assert.New(t)

	type givenModel struct {
		ID     uint   `persistence:"primary,type:serial"`
		Column string `persistence:"name:field"`
		Slice  []int
	}

	given := givenModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
	}

	expected := TableMeta{
		Name:   "givenModel",
		Type:   reflect.TypeOf((*givenModel)(nil)).Elem(),
		Schema: "persist",
		Columns: NewFieldMetasFrom(
			FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
			FieldMeta{Valid: true, Name: "Column", Type: StringType, value: given.Column, set: true, zero: false, Tag: Tag{Valid: true, Name: "field"}},
		),
		Primary: NewFieldMetasFrom(
			FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
		),
		Joins:           map[string][]TableMeta{},
		ManyToManyJoins: map[string][]TableMeta{},
	}

	actual := ObtainTableMeta(given, mockValidator)

	assert.Equals(expected, actual)
}

func TestObtainTableMetaWithJoins(t *testing.T) {
	assert := assert.New(t)

	type givenJoiner struct {
		ID    uint   `persistence:"primary,type:serial"`
		Field string `persistence:"name:column"`
	}

	type givenModel struct {
		ID      uint   `persistence:"primary,type:serial"`
		Column  string `persistence:"name:field"`
		Slice   []int
		Joiners []givenJoiner `persistence:"join"`
	}

	given := givenModel{
		ID:     23,
		Column: "test",
		Slice:  []int{1, 2, 3},
		Joiners: []givenJoiner{
			{
				ID:    4,
				Field: "value",
			}, {
				ID:    5,
				Field: "entry",
			},
		},
	}

	expected := TableMeta{
		Name:   "givenModel",
		Type:   reflect.TypeOf((*givenModel)(nil)).Elem(),
		Schema: "persist",
		Columns: NewFieldMetasFrom(
			FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
			FieldMeta{Valid: true, Name: "Column", Type: StringType, value: given.Column, set: true, zero: false, Tag: Tag{Valid: true, Name: "field"}},
		),
		Primary: NewFieldMetasFrom(
			FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
		),
		Joins: map[string][]TableMeta{
			"Joiners": {
				{
					Name:   "givenJoiner",
					Type:   reflect.TypeOf((*givenJoiner)(nil)).Elem(),
					Schema: "persist",
					Columns: NewFieldMetasFrom(
						FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.Joiners[0].ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
						FieldMeta{Valid: true, Name: "Field", Type: StringType, value: given.Joiners[0].Field, set: true, zero: false, Tag: Tag{Valid: true, Name: "column"}},
					),
					Primary: NewFieldMetasFrom(
						FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.Joiners[0].ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
					),
					Joins:           map[string][]TableMeta{},
					ManyToManyJoins: map[string][]TableMeta{},
				}, {
					Name:   "givenJoiner",
					Type:   reflect.TypeOf((*givenJoiner)(nil)).Elem(),
					Schema: "persist",
					Columns: NewFieldMetasFrom(
						FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.Joiners[1].ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
						FieldMeta{Valid: true, Name: "Field", Type: StringType, value: given.Joiners[1].Field, set: true, zero: false, Tag: Tag{Valid: true, Name: "column"}},
					),
					Primary: NewFieldMetasFrom(
						FieldMeta{Valid: true, Name: "ID", Type: UintType, value: given.Joiners[1].ID, set: true, zero: false, Tag: Tag{Valid: true, Type: "serial", PrimaryKey: true}},
					),
					Joins:           map[string][]TableMeta{},
					ManyToManyJoins: map[string][]TableMeta{},
				},
			},
		},
		ManyToManyJoins: map[string][]TableMeta{},
	}

	actual := ObtainTableMeta(given, mockValidator)

	assert.Equals(expected, actual)
}

func TestConstructFields(test *testing.T) {
	assert := assert.New(test)

	meta := ObtainTableMeta(model{}, mockValidator)
	t, v := typeAndValue(model{})

	actual := constructFields(t, v, mockValidator)

	assert.Equals(meta.Columns, actual)
}

func TestPromotedConstructFields(test *testing.T) {
	assert := assert.New(test)

	meta := ObtainTableMeta(struct{ model }{}, mockValidator)
	t, v := typeAndValue(struct{ model }{})

	actual := constructFields(t, v, mockValidator)

	assert.Equals(meta.Columns, actual)
}

func TestConstructFieldsFromMap(test *testing.T) {
	assert := assert.New(test)

	t, v := typeAndValue(map[string]interface{}{
		"field":  uint32(7),
		"column": int64(1),
	})

	actual := constructFields(t, v, mockValidator)
	expected := NewFieldMetasFrom(
		FieldMeta{Valid: true, Name: "column", value: int64(1), zero: false, set: true, Type: reflect.TypeOf((*int64)(nil)).Elem(), Tag: Tag{Valid: true}},
		FieldMeta{Valid: true, Name: "field", value: uint32(7), zero: false, set: true, Type: reflect.TypeOf((*uint32)(nil)).Elem(), Tag: Tag{Valid: true}},
	)
	assert.Equals(expected, actual)
}

func typeAndValue(i interface{}) (reflect.Type, reflect.Value) {
	return reflect.TypeOf(i), reflect.ValueOf(i)
}
