// Package persistence is a pure* Go sql dialect agnostic sql interface. It can be used as a raw query executor, query
// builder, or ORM. Persistence comes packaged with the dialects which can be used to interact with either postgres or
// sqlite databases. These are both optional to use.
//
// *the sqlite dialect driver, if used, requires cgo and is therefore not "pure". It is not required for persistence to
// be usable though.
package persistence // import "gitlab.com/cosban/persistence"

import (
	"database/sql"

	"gitlab.com/cosban/persistence/execution"
	"gitlab.com/cosban/persistence/internal"
)

// JoinKind specifies which type of join is to be used
type JoinKind uint32

const (
	// InnerJoin represents a full inner join on the database
	InnerJoin JoinKind = iota + 1
	// FullOuterJoin represents a full outer join on the database
	FullOuterJoin
	// LeftOuterJoin represents a left outer join on the database
	LeftOuterJoin
	// RightOuterJoin represents a right outer join on the database
	RightOuterJoin
)

// DialectDriver is the driver which the user set. Do this by blank importing the driver in your main class.
var (
	DialectDriver Driver
)

// Connection represents a connection to a database. Programs should keep hold of this connection because it is how
// all interaction within the database takesplace, whether through raw, or built up queries.
type Connection struct {
	db     execution.Database
	driver Driver
	stack  []execution.Transaction
}

// Open takes in a connection string and opens a wrapped connection from it
//    connection, err := persistence.Open("postgres", "postgres://user@localhost:5432/database?sslmode=disable")
// persistence contains a postgres driver built in, but the user may import any other driver as long as it implements
// the `Driver` interface while also setting themselves as a driver on init
//
//    import _ "gitlab.com/cosban/persistence/postgres"
//
//    // driver init example
//    package driver
//    func init() {
//       persistence.DialectDriver = &postgres{
//           schema: "public",
//       }
//    }
func Open(driver, connection string) (*Connection, error) {
	d, err := sql.Open(driver, connection)
	if err != nil {
		return nil, err
	}
	db := execution.NewDatabase(d)
	return newConnection(db), nil
}

// Close closes a database connection. Once a connection is closed, it may no longer be used. A new one must be
// created.
func (c *Connection) Close() {
	c.db.Close()
}

// newConnection takes in a database and builds a wrapped connection with it
func newConnection(db execution.Database) *Connection {
	return &Connection{
		db:     db,
		driver: DialectDriver,
		stack:  []execution.Transaction{},
	}
}

// IsConnected pings the database and returns true if the connection is still active
func (c *Connection) IsConnected() bool {
	err := c.db.Ping()
	return err == nil
}

// BeginTx starts a transaction
func (c *Connection) BeginTx() error {
	tx, err := c.db.Begin()
	if err != nil {
		return err
	}
	c.stack = append(c.stack, tx)
	return nil
}

// RollbackTx rolls back the last in progress transaction
func (c *Connection) RollbackTx() error {
	tx, err := c.pop()
	if err != nil {
		return err
	}
	return tx.Rollback()
}

// CommitTx commits the last in progress transaction
func (c *Connection) CommitTx() error {
	tx, err := c.pop()
	if err != nil {
		return err
	}
	return tx.Commit()
}

// RawStatement begins creation and execution of a query that is written out raw
func (c *Connection) RawStatement() *RawStatement {
	return &RawStatement{
		db: c.db,
		c:  c,
	}
}

// BuildStatement begins creation and execution of a query that is written out with the query builder
func (c *Connection) BuildStatement() *StatementBuilder {
	return &StatementBuilder{
		connection: c,
		pieces: QueryPieces{
			Selects: []string{},
			Filters: []Constraint{},
			Joins:   []JoinConstraint{},
			Order:   []string{},
			Groups:  []string{},
			Having:  []Constraint{},
			Args:    []interface{}{},
		},
	}
}

// SetDefaultSchema sets the default schema on the database being used. This is only needed if transactions need to be
// performed on a non-standard schema. If this is not set by the user, the default schema is set by the driver. This is
// usually set as `public` or `dbo`.
func (c *Connection) SetDefaultSchema(schema string) {
	c.driver.SetDefaultSchema(schema)
}

func (c *Connection) pop() (execution.Transaction, error) {
	if len(c.stack) < 1 {
		return nil, internal.ErrNoTxStarted
	}
	tx := c.stack[len(c.stack)-1]
	c.stack = c.stack[:len(c.stack)-1]
	return tx, nil
}

// ExecuteTx determines whether a commit or rollback needs to take place by checking whether any of the components which
// make up the latest transaction produced an error. A commit occurs on no error, otherwise a rollback takes place. The
// boolean value returned is an indicator of whether the transaction attempted to commit
// You may want to defer execution of a transaction
//    conn.BeginTx()
//    defer con.ExecuteTx()
//    // if errors occur during a transaction, they are recorded and `ExecuteTx` will rollback
//    err := /* perform query or execute statement */
//    if err != nil {
//        return err // `ExecuteTx` rolls back and error is returned
//    }
func (c *Connection) ExecuteTx() error {
	if len(c.stack) < 1 {
		return internal.ErrNoTxStarted
	}
	tx := c.stack[len(c.stack)-1]
	c.stack = c.stack[:len(c.stack)-1]
	return tx.Execute()
}

func (c *Connection) peek() execution.Transaction {
	if len(c.stack) > 0 {
		return c.stack[len(c.stack)-1]
	}
	return nil
}
