package postgres

import (
	"bytes"
	"fmt"
	"reflect"
	"time"

	_ "github.com/lib/pq" // since this is a postgres driver, we might as well import this
	"gitlab.com/cosban/persistence"
	"gitlab.com/cosban/persistence/statement"
)

type postgres struct {
	persistence.CommonDriver
}

var (
	typesToColumn = map[reflect.Type]string{
		reflect.TypeOf((*bool)(nil)):      "BOOLEAN",
		reflect.TypeOf((*int16)(nil)):     "SMALLINT",
		reflect.TypeOf((*int32)(nil)):     "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "BIGINT",
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*uint32)(nil)):    "INTEGER",
		reflect.TypeOf((*uint64)(nil)):    "BIGINT",
		reflect.TypeOf((*uint)(nil)):      "INTEGER",
		reflect.TypeOf((*string)(nil)):    "TEXT",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP",
	}
	generatedTypes = map[reflect.Type]string{
		reflect.TypeOf((*int)(nil)):       "SERIAL",
		reflect.TypeOf((*int16)(nil)):     "SMALLSERIAL",
		reflect.TypeOf((*int32)(nil)):     "SERIAL",
		reflect.TypeOf((*int64)(nil)):     "BIGSERIAL",
		reflect.TypeOf((*uint)(nil)):      "SERIAL",
		reflect.TypeOf((*uint16)(nil)):    "SMALLSERIAL",
		reflect.TypeOf((*uint32)(nil)):    "SERIAL",
		reflect.TypeOf((*uint64)(nil)):    "BIGSERIAL",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP NOT NULL DEFAULT now()",
	}
)

func init() {
	driver := (&postgres{}).WithSchema("public")
	driver.SetBindFunc(driver.Bind)
	driver.SetIsValidTypeFunc(driver.IsValidType)
	driver.SetToColumnTypeFunc(driver.ToColumnType)
	persistence.DialectDriver = driver
}

func (pg *postgres) WithSchema(schema string) *postgres {
	pg.SetDefaultSchema(schema)
	return pg
}

func (pg *postgres) HasTable(meta persistence.TableMeta) statement.Statement {
	return persistence.Prepare(
		`SELECT EXISTS (
			SELECT 1
			FROM information_schema.tables
			WHERE table_schema = LOWER($1)
			AND table_name = LOWER($2)
		);`, pg.GetSchema(meta), meta.Name,
	)
}

func (pg *postgres) GetTableColumns(meta persistence.TableMeta) statement.Statement {
	return persistence.Prepare(
		`SELECT column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = LOWER($1)
			AND table_name = LOWER($2);`,
		pg.GetSchema(meta), meta.Name,
	)
}

func (*postgres) Bind(length int) string {
	return fmt.Sprintf("$%d", length)
}

func (pg *postgres) Insert(meta persistence.TableMeta) statement.Statement {
	var buffer bytes.Buffer
	stmt := pg.CommonDriver.Insert(meta)
	buffer.WriteString(stmt.Query())
	returning := pg.returning(meta)
	buffer.WriteString(returning)
	return persistence.PrepareQuery(buffer.String(), stmt.Args()...)
}

func (*postgres) returning(meta persistence.TableMeta) string {
	var buffer bytes.Buffer
	prefix := " RETURNING "
	columns := meta.Columns.Iter()
	for columns.Next() {
		_, column := columns.Current()
		if column.Tag.Generated {
			buffer.WriteString(prefix)
			buffer.WriteString(column.GetName())
			prefix = ", "
		}
	}
	return buffer.String()
}

func (pg *postgres) Update(meta persistence.TableMeta, constraints ...persistence.Constraint) statement.Statement {
	var buffer bytes.Buffer
	stmt := pg.CommonDriver.Update(meta, constraints...)
	buffer.WriteString(stmt.Query())
	if meta.Primary.Size() > 0 {
		returning := pg.returning(meta)
		buffer.WriteString(returning)
	}
	return persistence.PrepareQuery(buffer.String(), stmt.Args()...)
}

func (pg *postgres) Upsert(meta persistence.TableMeta) statement.Statement {
	var buffer bytes.Buffer
	stmt := pg.CommonDriver.Upsert(meta)
	buffer.WriteString(stmt.Query())
	if meta.Primary.Size() > 0 {
		returning := pg.returning(meta)
		buffer.WriteString(returning)
	}
	return persistence.PrepareQuery(buffer.String(), stmt.Args()...)
}

func (*postgres) IsValidType(t reflect.Type) bool {
	if t.Kind() != reflect.Ptr {
		t = reflect.New(t).Type()
	}
	_, ok := typesToColumn[t]
	return ok
}

func (*postgres) ToColumnType(column persistence.FieldMeta) string {
	t := column.Type
	if column.Type.Kind() != reflect.Ptr {
		t = reflect.New(column.Type).Type()
	}
	if column.Tag.Generated {
		return generatedTypes[t]
	}
	if column.Tag.Type == "" {
		return typesToColumn[t]
	}
	return column.Tag.Type
}

func (*postgres) RowIDField() persistence.FieldMeta {
	err := fmt.Errorf("Operation `RowIDField` not supported by postgres driver")
	panic(err)
}
