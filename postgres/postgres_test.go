package postgres

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
	"gitlab.com/cosban/persistence"
)

type table struct {
	ID          int          `persistence:"primary,generated,name:id"`
	Flag        bool         `persistence:"not_null"`
	Deleted     *time.Time   `persistence:"name:deleted"`
	Others      []otherTable `persistence:"join"`
	OtherOthers []otherTable `persistence:"many_to_many"`
	Updated     *time.Time   `persistence:"generated"`
}

type otherTable struct {
	ID int `persistence:"primary_key,name:id"`
}

func TestIsValidType(t *testing.T) {
	driver := persistence.DialectDriver
	given := reflect.TypeOf((*string)(nil))

	actual := driver.IsValidType(given)
	assert.True(t, actual)

	actual = driver.IsValidType(given.Elem())
	assert.True(t, actual)
}

func TestHasTable(t *testing.T) {
	driver := persistence.DialectDriver
	meta := persistence.ObtainTableMeta(table{}, mockValidator)
	actual := driver.HasTable(meta)
	expected := persistence.Prepare(
		`SELECT EXISTS (
			SELECT 1
			FROM information_schema.tables
			WHERE table_schema = LOWER($1)
			AND table_name = LOWER($2)
		);`, "persist", meta.Name,
	)
	assert.Equals(t, expected, actual)
}

func TestGetTableColumns(t *testing.T) {
	driver := persistence.DialectDriver
	meta := persistence.ObtainTableMeta(table{}, mockValidator)
	actual := driver.GetTableColumns(meta)
	expected := persistence.Prepare(
		`SELECT column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = LOWER($1)
			AND table_name = LOWER($2);`, "persist", meta.Name,
	)
	assert.Equals(t, expected, actual)
}

func TestBind(t *testing.T) {
	driver := persistence.DialectDriver
	var actual, expected string
	actual = driver.Bind(1)
	expected = "$1"
	assert.Equals(t, expected, actual)

	actual = driver.Bind(12)
	expected = "$12"
	assert.Equals(t, expected, actual)
}

func TestInsertModel(t *testing.T) {
	driver := persistence.DialectDriver
	given := table{ID: 12, Flag: false}
	meta := persistence.ObtainTableMeta(&given, mockValidator)
	actual := driver.Insert(meta)
	expected := persistence.PrepareQuery("INSERT INTO persist.table (id, Flag) VALUES ($1, $2) RETURNING id, Updated", 12, false)
	assert.Equals(t, expected, actual)
}

func TestUpdateModel(t *testing.T) {
	driver := persistence.DialectDriver
	given := table{ID: 12, Flag: false}
	meta := persistence.ObtainTableMeta(&given, mockValidator)
	actual := driver.Update(meta)
	expected := persistence.PrepareQuery("UPDATE persist.table SET Flag = $1, deleted = $2 WHERE persist.table.id = $3 RETURNING id, Updated", false, (*time.Time)(nil), 12)
	assert.Equals(t, expected, actual)
}

func TestUpdateModelWithConstraints(t *testing.T) {
	driver := persistence.DialectDriver
	given := table{Flag: false}
	meta := persistence.ObtainTableMeta(&given, mockValidator)
	constraints := []persistence.Constraint{
		{
			Section: "Flag = ?",
			Kind:    "WHERE",
			Args:    []interface{}{true},
		},
	}
	actual := driver.Update(meta, constraints...)
	expected := persistence.PrepareQuery("UPDATE persist.table SET Flag = $1, deleted = $2 WHERE Flag = $3 RETURNING id, Updated", false, (*time.Time)(nil), true)
	assert.Equals(t, expected, actual)
}

func TestUpsertModel(t *testing.T) {
	driver := persistence.DialectDriver
	given := table{ID: 12, Flag: false}
	meta := persistence.ObtainTableMeta(&given, mockValidator)
	actual := driver.Upsert(meta)

	expected := persistence.PrepareQuery(
		"INSERT INTO persist.table (id, Flag) VALUES ($1, $2) ON CONFLICT (id) DO UPDATE SET Flag = $3, deleted = $4 WHERE persist.table.id = $5 RETURNING id, Updated",
		12, false, false, (*time.Time)(nil), 12,
	)
	assert.Equals(t, expected, actual)
}

var mockValidator = mockTypeValidator{}

type mockTypeValidator struct{}

func (mockTypeValidator) IsValidType(t reflect.Type) bool {
	return true
}

func (mockTypeValidator) GetSchema() string {
	return "persist"
}
