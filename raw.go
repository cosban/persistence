package persistence

import (
	"database/sql"

	"gitlab.com/cosban/persistence/execution"
	"gitlab.com/cosban/persistence/internal"
	"gitlab.com/cosban/persistence/statement"
)

// RawStatement is used to create raw queries or statements
type RawStatement struct {
	db execution.Database
	c  *Connection
}

// ExecutedResult provides the metadata about the results of a statement that has been executed
type ExecutedResult struct {
	HasRows   bool
	HasResult bool
	Rows      *sql.Rows
	Result    sql.Result
}

// PrepareQuery builds a statement to be used in a query
func PrepareQuery(query string, args ...interface{}) statement.Statement {
	return statement.New(query, args, false)
}

// Prepare builds a statement to be executed
func Prepare(query string, args ...interface{}) statement.Statement {
	return statement.New(query, args, true)
}

// PrepareAndExecute performs the actions of Prepare and Execute a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func (r *RawStatement) PrepareAndExecute(query string, args ...interface{}) (sql.Result, error) {
	result, err := r.db.Exec(query, args...)
	return result, internal.Error(err, statement.New(query, args, true))
}

// PrepareAndQuery performs the actions of Prepare and Query a single statement
// in one step rather than forcing the user to manually Prepare a statement.
func (r *RawStatement) PrepareAndQuery(query string, args ...interface{}) (*sql.Rows, error) {
	rows, err := r.db.Query(query, args...)
	return rows, internal.Error(err, statement.New(query, args, false))
}

// BlindlyExecuteStatements allows the user to execute or query >= 1 statement in a single sql transaction but only
// returns error info
func (r *RawStatement) BlindlyExecuteStatements(stmts ...statement.Statement) error {
	_, err := r.ExecuteStatements(stmts...)
	return err
}

// ExecuteStatements allows the user to execute or query >= 1 statement in a single sql transaction
func (r *RawStatement) ExecuteStatements(stmts ...statement.Statement) ([]ExecutedResult, error) {
	if r.c.peek() == nil {
		r.c.BeginTx()
		defer r.c.ExecuteTx()
	}
	tx := r.c.peek()
	var results []ExecutedResult
	for _, stmt := range stmts {
		var err error
		var result sql.Result
		var rows *sql.Rows
		if stmt.ToExec() {
			if result, err = tx.Exec(stmt.Query(), stmt.Args()...); err != nil {
				tx.Rollback()
				return nil, internal.Error(err, stmt)
			}
		} else {
			if rows, err = tx.Query(stmt.Query(), stmt.Args()...); err != nil {
				tx.Rollback()
				return nil, internal.Error(err, stmt)
			}
		}
		executedResult := ExecutedResult{
			HasResult: result != nil,
			Result:    result,
			HasRows:   rows != nil,
			Rows:      rows,
		}
		results = append(results, executedResult)
	}
	return results, tx.Execute()
}

// Query allows the user to perform a query where >= 1 rows are expected back
// It simply returns the rows so that the user may manipulate the data as they please
func (r *RawStatement) Query(stmt statement.Statement) (*sql.Rows, error) {
	rows, err := r.db.Query(stmt.Query(), stmt.Args()...)
	return rows, internal.Error(err, stmt)
}

// Execute allows the user to execute a statement it returns the result of the execution
func (r *RawStatement) Execute(stmt statement.Statement) (sql.Result, error) {
	result, err := r.db.Exec(stmt.Query(), stmt.Args()...)
	return result, internal.Error(err, stmt)
}

// QueryRow allows the user to perform an sql query where only one row is expected
// The results of the query are put into the passed in data interfaces
func (r *RawStatement) QueryRow(stmt statement.Statement, data ...interface{}) error {
	row, err := r.Query(stmt)
	if err != nil {
		return internal.Error(err, stmt)
	}
	defer row.Close()
	if row.Next() {
		err = row.Scan(data...)
	} else {
		err = internal.ErrNoRows
	}
	return internal.Error(err, stmt)
}
