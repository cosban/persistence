package sqlite3

import (
	"reflect"
	"time"

	_ "github.com/mattn/go-sqlite3" // since this is a sqlite driver, we might as well import this
	"gitlab.com/cosban/persistence"
	"gitlab.com/cosban/persistence/statement"
)

var (
	typesToColumn = map[reflect.Type]string{
		reflect.TypeOf((*bool)(nil)):      "INTEGER",
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*int16)(nil)):     "INTEGER",
		reflect.TypeOf((*int32)(nil)):     "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "INTEGER",
		reflect.TypeOf((*bool)(nil)):      "INTEGER",
		reflect.TypeOf((*uint)(nil)):      "INTEGER",
		reflect.TypeOf((*uint16)(nil)):    "INTEGER",
		reflect.TypeOf((*uint32)(nil)):    "INTEGER",
		reflect.TypeOf((*uint64)(nil)):    "INTEGER",
		reflect.TypeOf((*string)(nil)):    "TEXT",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP",
	}
	generatedTypes = map[reflect.Type]string{
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*int16)(nil)):     "INTEGER",
		reflect.TypeOf((*int32)(nil)):     "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "INTEGER",
		reflect.TypeOf((*uint)(nil)):      "INTEGER",
		reflect.TypeOf((*uint16)(nil)):    "INTEGER",
		reflect.TypeOf((*uint32)(nil)):    "INTEGER",
		reflect.TypeOf((*uint64)(nil)):    "INTEGER",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
	}
)

func init() {
	driver := &sqlite3{}
	driver.SetBindFunc(driver.Bind)
	driver.SetIsValidTypeFunc(driver.IsValidType)
	driver.SetToColumnTypeFunc(driver.ToColumnType)
	persistence.DialectDriver = driver
}

type sqlite3 struct {
	persistence.CommonDriver
}

func (*sqlite3) HasTable(meta persistence.TableMeta) statement.Statement {
	return persistence.Prepare(
		`SELECT COUNT(name) FROM sqlite_master WHERE type='table' AND name= ?;`, meta.Name,
	)
}

func (*sqlite3) GetTableColumns(meta persistence.TableMeta) statement.Statement {
	return persistence.Prepare(`SELECT LOWER(name) AS name, type FROM pragma_table_info(?);`, meta.Name)
}

func (*sqlite3) Bind(length int) string {
	return "?"
}

func (*sqlite3) IsValidType(t reflect.Type) bool {
	if t.Kind() != reflect.Ptr {
		t = reflect.New(t).Type()
	}
	_, ok := typesToColumn[t]
	return ok
}

func (*sqlite3) ToColumnType(column persistence.FieldMeta) string {
	t := column.Type
	if column.Type.Kind() != reflect.Ptr {
		t = reflect.New(column.Type).Type()
	}
	if column.Tag.Generated {
		return generatedTypes[t]
	}
	if column.Tag.Type == "" {
		return typesToColumn[t]
	}
	return column.Tag.Type
}

func (*sqlite3) RowIDField() persistence.FieldMeta {
	return persistence.FieldMeta{
		Name:     "rowid",
		Type:     reflect.TypeOf((*int)(nil)),
		DataType: "INTEGER PRIMARY KEY",
		Tag: persistence.Tag{
			Valid:       true,
			NotNullable: true,
			Unique:      true,
			PrimaryKey:  true,
			Generated:   true,
		},
		Valid: true,
	}
}
