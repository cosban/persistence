package sqlite3

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
	"gitlab.com/cosban/persistence"
)

func TestIsValidType(t *testing.T) {
	driver := persistence.DialectDriver
	given := reflect.TypeOf((*string)(nil))

	actual := driver.IsValidType(given)
	assert.True(t, actual)

	actual = driver.IsValidType(given.Elem())
	assert.True(t, actual)
}

func TestHasTable(t *testing.T) {
	driver := persistence.DialectDriver
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	meta := persistence.ObtainTableMeta(table{}, mockValidator)
	actual := driver.HasTable(meta)
	expected := persistence.Prepare(`SELECT COUNT(name) FROM sqlite_master WHERE type='table' AND name= ?;`, meta.Name)
	assert.Equals(t, expected, actual)
}

func TestGetTableColumns(t *testing.T) {
	driver := persistence.DialectDriver
	type table struct {
		ID      int        `persistence:"primary,generated,name:id"`
		Flag    bool       `persistence:"not_null"`
		Deleted *time.Time `persistence:"name:deleted"`
		Updated *time.Time `persistence:"generated"`
	}
	meta := persistence.ObtainTableMeta(table{}, mockValidator)
	actual := driver.GetTableColumns(meta)
	expected := persistence.Prepare(`SELECT LOWER(name) AS name, type FROM pragma_table_info(?);`, meta.Name)
	assert.Equals(t, expected, actual)
}

func TestBind(t *testing.T) {
	driver := persistence.DialectDriver
	var actual, expected string
	actual = driver.Bind(1)
	expected = "?"
	assert.Equals(t, expected, actual)

	actual = driver.Bind(12)
	expected = "?"
	assert.Equals(t, expected, actual)
}

type Role struct {
	ID   uint32 `persistence:"primary,generated"`
	User []User `persistence:"many_to_many"`
}
type User struct {
	ID    uint32 `persistence:"primary,generated"`
	Roles []Role `persistence:"many_to_many"`
}

var mockValidator = mockTypeValidator{}



type mockTypeValidator struct{}

func (mockTypeValidator) IsValidType(t reflect.Type) bool {
	typesToColumn = map[reflect.Type]string{
		reflect.TypeOf((*bool)(nil)):      "INTEGER",
		reflect.TypeOf((*int)(nil)):       "INTEGER",
		reflect.TypeOf((*int64)(nil)):     "INTEGER",
		reflect.TypeOf((*string)(nil)):    "TEXT",
		reflect.TypeOf((*time.Time)(nil)): "TIMESTAMP",
	}
	if t.Kind() != reflect.Ptr {
		t = reflect.New(t).Type()
	}
	_, ok := typesToColumn[t]
	return ok
}

func (mockTypeValidator) GetSchema() string {
	return "persist"
}
