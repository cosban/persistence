package persistence

import (
	"bytes"
	"database/sql"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/cosban/persistence/internal"
	"gitlab.com/cosban/persistence/statement"
)

// StatementBuilder is used to build and execute a query
type StatementBuilder struct {
	connection *Connection
	meta       TableMeta
	pieces     QueryPieces
	includes   []Export
	raw        bytes.Buffer
}

// QueryPieces is the structured representation of a query
type QueryPieces struct {
	Table   string
	Schema  string
	Selects []string
	Filters []Constraint
	Joins   []JoinConstraint
	Limit   int
	Offset  int
	Order   []string
	Groups  []string
	Having  []Constraint
	Args    []interface{}
}

// Constraint is used to modify the results of a query, either by filtering, ordering, or joining
type Constraint struct {
	Section string
	Kind    string
	Args    []interface{}
}

type columnRetriever func(meta ...TableMeta) ([]interface{}, []string)

// CreateFromModel takes in one or more models and attmepts to create tables based on their properties and tags. For
// a field to become a column, it must be of a valid type (as specified by the driver), or a valid type within an
// anonymous member
func (query *StatementBuilder) CreateFromModel(models ...interface{}) error {
	stmts, metas := query.createFromModel(models...)
	multiJoins := query.createMultiJoinTables(metas)
	stmts = append(stmts, multiJoins...)
	return query.connection.RawStatement().BlindlyExecuteStatements(stmts...)
}

func (query *StatementBuilder) createFromModel(models ...interface{}) ([]statement.Statement, map[string]TableMeta) {
	stmts := []statement.Statement{}
	metas := map[string]TableMeta{}
	for _, model := range models {
		meta := ObtainTableMeta(model, query)
		stmt := query.connection.driver.CreateTable(meta)
		stmts = append(stmts, stmt)
		metas[meta.Name] = meta
	}
	return stmts, metas
}

func (query *StatementBuilder) createMultiJoinTables(metas map[string]TableMeta) []statement.Statement {
	stmts := []statement.Statement{}
	for _, meta := range metas {
		for name := range meta.ManyToManyJoins {
			if join, exists := obtainTableMetaFromSliceField(meta, name, query); exists {
				if value, ok := metas[join.Name]; ok {
					stmt := query.createMultiJoinFromMeta(meta, value)
					stmts = append(stmts, stmt)
				} else if tableExists, err := query.hasTable(join); err == nil && tableExists {
					stmt := query.createMultiJoinFromMeta(meta, join)
					stmts = append(stmts, stmt)
				} else if err != nil {
					panic(err)
				}
			}
		}
	}
	return stmts
}

func (query *StatementBuilder) createOrUpdateMultiJoinTables(meta TableMeta) []statement.Statement {
	stmts := []statement.Statement{}
	for name := range meta.ManyToManyJoins {
		if join, exists := obtainTableMetaFromSliceField(meta, name, query); exists {
			if tableExists, err := query.hasTable(join); err == nil && tableExists {
				multiMeta := obtainMultiJoinTableMeta(meta, join, query.GetSchema())
				if multiExists, err := query.hasTable(multiMeta); err == nil {
					if multiExists {
						query.migrateTable(multiMeta)
					} else {
						stmt := query.createMultiJoinFromMeta(meta, join)
						stmts = append(stmts, stmt)
					}
				} else {
					panic(err)
				}
			}
		}
	}
	return stmts
}

func (query *StatementBuilder) createMultiJoinFromMeta(first, second TableMeta) statement.Statement {
	meta := obtainMultiJoinTableMeta(first, second, query.GetSchema())
	return query.connection.driver.CreateTable(meta)
}

// HasTable queries the database to check the existence of any table with the name that the supplied model would use
// if any error occurs, `HasTable` will return false and that error regardless of whether the table actually exists.
func (query *StatementBuilder) HasTable(model interface{}) (bool, error) {
	meta := ObtainTableMeta(model, query)
	return query.hasTable(meta)
}

func (query *StatementBuilder) hasTable(meta TableMeta) (bool, error) {
	var exists bool
	stmt := query.connection.driver.HasTable(meta)
	if err := query.connection.RawStatement().QueryRow(stmt, &exists); err != nil {
		return false, err
	}
	return exists, nil
}

func (query *StatementBuilder) getTableColumns(meta TableMeta) (map[string]FieldMeta, error) {
	stmt := query.connection.driver.GetTableColumns(meta)
	rows, err := query.connection.RawStatement().Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	columns := map[string]FieldMeta{}
	for rows.Next() {
		var name, kind string
		if err := rows.Scan(&name, &kind); err != nil {
			return nil, err
		}
		columns[name] = FieldMeta{
			Name:     name,
			DataType: kind,
		}
	}
	return columns, nil
}

func (query *StatementBuilder) migrateTable(meta TableMeta) error {
	cols, err := query.getTableColumns(meta)
	if err != nil {
		return err
	}
	stmts := []statement.Statement{}
	columns := meta.Columns.Iter()
	for columns.Next() {
		_, column := columns.Current()
		name := strings.ToLower(column.GetName())
		if _, ok := cols[name]; !ok {
			if query.IsValidType(column.Type) {
				stmt := query.connection.driver.AddTableColumn(meta, column)
				stmts = append(stmts, stmt)
			}
		}
	}
	multiJoins := query.createOrUpdateMultiJoinTables(meta)
	stmts = append(stmts, multiJoins...)
	return query.connection.RawStatement().BlindlyExecuteStatements(stmts...)
}

// DropFromModel will attempt to drop the tables represented by the given models. It will return the first
// encountered error if any are seen during this process.
func (query *StatementBuilder) DropFromModel(models ...interface{}) error {
	dropFunction := func(meta TableMeta, constraints ...Constraint) statement.Statement {
		return query.connection.driver.DropTable(meta)
	}
	stmts := query.executeOnMeta(dropFunction, models...)
	return query.connection.RawStatement().BlindlyExecuteStatements(stmts...)
}

func (query *StatementBuilder) processInputs(driverFunc func(TableMeta, ...Constraint) statement.Statement, inputs ...interface{}) error {
	for _, input := range inputs {
		meta := ObtainTableMeta(input, query)
		if err := query.buildQueryScan(&meta, driverFunc, query.pieces.Filters...); err != nil {
			return err
		}
		meta.Export(input)
		if err := query.upsertJoins(input, meta); err != nil {
			return err
		}
	}
	return nil
}

func (query *StatementBuilder) buildQueryScan(
	meta *TableMeta,
	build func(TableMeta, ...Constraint) statement.Statement,
	constraints ...Constraint,
) error {
	stmt := build(*meta, constraints...)
	if stmt.ToExec() {
		result, err := query.connection.RawStatement().Execute(stmt)
		if err != nil {
			return err
		}
		rowid := query.metaRowIDField(*meta)
		last, err := result.LastInsertId()
		if err != nil {
			return err
		}
		selects := selectGeneratedFields(*meta)
		if len(selects) == 0 || meta.JoinTable {
			return nil
		}
		constraint := fmt.Sprintf("%s = ?", rowid.Name)
		if rowid.Zero() {
			stmt = query.connection.BuildStatement().Select(selects...).Where(constraint, last).queryStatement(*meta)
		} else {
			stmt = query.connection.BuildStatement().Select(selects...).Where(constraint, rowid.Value()).queryStatement(*meta)
		}
	}
	rows, err := query.connection.RawStatement().Query(stmt)
	if err != nil {
		return err
	}
	defer rows.Close()
	return query.scanTableMeta(meta, rows, query.scannableGeneratedFields)
}

func (query *StatementBuilder) metaRowIDField(meta TableMeta) FieldMeta {
	driverField := query.connection.driver.RowIDField()
	primary := meta.Primary.Iter()
	for primary.Next() {
		_, v := primary.Current()
		if v.Tag.Generated {
			return v
		}
	}
	return driverField
}

func selectGeneratedFields(meta TableMeta) []string {
	selects := []string{}
	columns := meta.Columns.Iter()
	for columns.Next() {
		_, column := columns.Current()
		if column.Tag.Generated {
			selects = append(selects, column.GetName())
		}
	}
	return selects
}

// Insert attempts to insert all of the provided models. If a join or multi-join are present within a model. If Include
// was called before insert, then persistence will try to insert each value found within the include as well.
func (query *StatementBuilder) Insert(inserts ...interface{}) error {
	insertFunc := func(meta TableMeta, constraints ...Constraint) statement.Statement {
		return query.connection.driver.Insert(meta)
	}
	return query.processInputs(insertFunc, inserts...)
}

// Update attempts to update all of the provided models. If a join or multi-join are present within a model, Update
// will attempt to insert the joined items as well if they do not already exist.
func (query *StatementBuilder) Update(updates ...interface{}) error {
	return query.processInputs(query.connection.driver.Update, updates...)
}

// Upsert attempts to insert the provided items if they do not exist in the table yet, otherwise updating those
// existing items. Like `Insert` and `Update`, if joins are present, it will attempt to insert them should they
// not exist.
func (query *StatementBuilder) Upsert(upserts ...interface{}) error {
	upsertFunc := func(meta TableMeta, constraints ...Constraint) statement.Statement {
		return query.connection.driver.Upsert(meta)
	}
	return query.processInputs(upsertFunc, upserts...)
}

func (query *StatementBuilder) upsert(meta *TableMeta) error {
	upsertFunc := func(meta TableMeta, constraints ...Constraint) statement.Statement {
		return query.connection.driver.Upsert(meta)
	}
	return query.buildQueryScan(meta, upsertFunc)
}

func (query *StatementBuilder) upsertJoins(out interface{}, meta TableMeta) error {
	for _, included := range query.includes {
		if joins, ok := meta.Joins[included.Name]; ok {
			for _, join := range joins {
				if err := query.upsert(&join); err != nil {
					return err
				}
				join.Export(out)
			}
		} else if joins, ok := meta.ManyToManyJoins[included.Name]; ok {
			for _, join := range joins {
				if !join.Zero {
					if err := query.upsert(&join); err != nil {
						return err
					}
					join.Export(out)
					multi := obtainMultiJoinTableMeta(meta, join, query.GetSchema())
					if err := query.upsert(&multi); err != nil {
						return err
					}
				}
			}
		}
		// FIXME: reaching this point should return an error
	}
	return nil
}

// Delete will attempt to delete any rows represented by the supplied models. This cascades for joins as well.
func (query *StatementBuilder) Delete(deletes ...interface{}) error {
	stmts := query.executeOnMeta(query.connection.driver.Delete, deletes...)
	return query.connection.RawStatement().BlindlyExecuteStatements(stmts...)
}

func (query *StatementBuilder) executeOnMeta(fn func(TableMeta, ...Constraint) statement.Statement, models ...interface{}) []statement.Statement {
	stmts := []statement.Statement{}
	for _, model := range models {
		meta := ObtainTableMeta(model, query)
		stmt := fn(meta, query.pieces.Filters...)
		stmts = append(stmts, stmt)
	}
	return stmts
}

// Select will limit the fields which are populated in the event that the user does not want the entire model to be
// populated
func (query *StatementBuilder) Select(selects ...string) *StatementBuilder {
	query.pieces.Selects = append(query.pieces.Selects, selects...)
	return query
}

// From is used to determine which table is queried from
func (query *StatementBuilder) From(i interface{}) *StatementBuilder {
	query.meta = ObtainTableMeta(i, query)
	return query
}

// Where will set a constraint on the query that is being built. Where must be called before `And`/`Or` are used.
// Constraints are not yet grouped, so this only allows for simple constraints to be used.
func (query *StatementBuilder) Where(section string, args ...interface{}) *StatementBuilder {
	if len(query.pieces.Filters) > 0 {
		panic("`Where` can only be called ONCE")
	}
	return query.constraint(section, "WHERE", args...)
}

// And adds an additional AND constraint to the query. Constraints are not yet grouped, so this only allows for simple
// constraints to be used.
func (query *StatementBuilder) And(section string, args ...interface{}) *StatementBuilder {
	if len(query.pieces.Filters) < 1 {
		panic("Call `Where` before calling `And`")
	}
	return query.constraint(section, "AND", args...)
}

// Or adds an additional OR constraint to the query. Constraints are not yet grouped, so this only allows for simple
// constraints to be used.
func (query *StatementBuilder) Or(section string, args ...interface{}) *StatementBuilder {
	if len(query.pieces.Filters) < 1 {
		panic("Call `Where` before calling `Or`")
	}
	return query.constraint(section, "OR", args...)
}

func (query *StatementBuilder) constraint(section, kind string, args ...interface{}) *StatementBuilder {
	query.pieces.Filters = append(
		query.pieces.Filters,
		Constraint{
			Section: section,
			Kind:    kind,
			Args:    args,
		},
	)
	return query
}

func representSameType(field reflect.StructField, modelType reflect.Type) bool {
	if !isSliceField(field) {
		return false
	}
	fieldType := field.Type.Elem()
	if fieldType.Kind() == reflect.Ptr {
		fieldType = fieldType.Elem()
	}
	if modelType.Kind() == reflect.Ptr {
		modelType = modelType.Elem()
	}
	return fieldType == modelType
}

func isSliceField(field reflect.StructField) bool {
	return (field.Type.Kind() == reflect.Slice || (field.Type.Kind() == reflect.Ptr && field.Type.Elem().Kind() == reflect.Slice))
}

// Order specifies the order in which results from the query should be returned
func (query *StatementBuilder) Order(order ...string) *StatementBuilder {
	query.pieces.Order = append(query.pieces.Order, order...)
	return query
}

// Limit ensures that no more than the provided number of results will be returned when the query is executed
func (query *StatementBuilder) Limit(limit int) *StatementBuilder {
	query.pieces.Limit = limit
	return query
}

// Offset skips over the first N results in the query where N is the number provided
func (query *StatementBuilder) Offset(offset int) *StatementBuilder {
	query.pieces.Offset = offset
	return query
}

// Schema sets the schema to be used in this query for times when usage of the default schema is not desired
func (query *StatementBuilder) Schema(schema string) *StatementBuilder {
	query.pieces.Schema = schema
	return query
}

// Query causes a statement to actually execute and sets the underlying data on to the provided interface
func (query *StatementBuilder) Query(i interface{}) error {
	return query.query(i)
}

func (query *StatementBuilder) query(out interface{}) error {
	rv := reflect.ValueOf(out)
	if rv.IsNil() {
		return internal.ErrNilExportProvided
	}
	if rv.Kind() != reflect.Ptr && (rv.Elem().Kind() != reflect.Struct || rv.Elem().Kind() != reflect.Slice) {
		return internal.ErrInvalidOutInterface
	}
	if query.meta.IsEmpty() {
		query.meta = DeriveTableMetaFromExport(out, query)
	}
	stmt := query.queryStatement(query.meta)
	if rv.Kind() != reflect.Ptr {
		return internal.Error(internal.ErrInvalidOutInterface, stmt)
	}
	rows, err := query.connection.RawStatement().Query(stmt)
	if err != nil {
		return err
	}
	defer rows.Close()
	kind := rv.Elem().Kind()
	if kind == reflect.Slice {
		if err := query.populateSlice(out, rows); err != nil {
			return internal.Error(err, stmt)
		}
	} else if kind == reflect.Struct {
		if err := query.populateStruct(out, rows); err != nil {
			return internal.Error(err, stmt)
		}
	}
	return nil
}

func (query *StatementBuilder) queryStatement(meta TableMeta) statement.Statement {
	query.pieces.Selects = query.buildSelects(meta, query.pieces.Joins)
	return query.connection.driver.Query(meta, query.pieces)
}

// Count causes a statement to actually execute and returns the number of rows that were returned as a result of the
// query
func (query *StatementBuilder) Count() (int, error) {
	var count = struct {
		Count int `persistence:"name:count(*) as count"`
	}{}
	err := query.Select("COUNT(*) AS count").query(&count)
	return count.Count, err
}

func (query *StatementBuilder) populateInterface(out *TableMeta, rows *sql.Rows) error {
	return query.scanTableMeta(out, rows, query.scannableColumns)
}

func (query *StatementBuilder) populateInterfaceGeneratedFields(out *TableMeta, rows *sql.Rows) error {
	return query.scanTableMeta(out, rows, query.scannableGeneratedFields)
}

func (query *StatementBuilder) scanTableMeta(out *TableMeta, rows *sql.Rows, getColumns columnRetriever) error {
	metas := []TableMeta{*out}
	for _, join := range query.pieces.Joins {
		if _, ok := out.Joins[join.Name]; ok {
			metas = append(metas, join.Meta)
		} else if _, ok := out.ManyToManyJoins[join.Name]; ok {
			metas = append(metas, join.Meta)
		}
	}
	columns, _ := getColumns(metas...)
	firstRead := true
	for rows.Next() {
		err := rows.Scan(columns...)
		if err != nil {
			return err
		}
		names, _ := rows.Columns()
		if firstRead {
			fillTableMeta(out, names, columns)
			if len(metas) == 1 {
				break
			}
		}
		fillJoinTableMetas(out, names, columns, query.pieces.Joins)
	}
	return nil
}

func (query *StatementBuilder) populateStruct(out interface{}, rows *sql.Rows) error {
	exports := extractExports(query.pieces.Joins)
	if reflect.TypeOf(out).Elem() == query.meta.Type {
		if err := query.populateInterface(&query.meta, rows); err != nil {
			return err
		}
		query.meta.Export(out, exports...)
	} else {
		outMeta := ObtainTableMeta(out, query)
		if err := query.populateInterface(&outMeta, rows); err != nil {
			return err
		}
		outMeta.Export(out, exports...)
	}
	return nil
}

func (query *StatementBuilder) populateSlice(out interface{}, rows *sql.Rows) error {
	rv := reflect.ValueOf(out)
	metas := []TableMeta{query.meta}
	for _, join := range query.pieces.Joins {
		if _, ok := query.meta.Joins[join.Name]; ok {
			metas = append(metas, join.Meta)
		} else if _, ok := query.meta.ManyToManyJoins[join.Name]; ok {
			metas = append(metas, join.Meta)
		}
	}
	columns, names := query.scannableColumns(metas...)
	for rows.Next() {
		err := rows.Scan(columns...)
		if err != nil {
			return err
		}
		v := reflect.New(query.meta.Type)
		for i, column := range columns {
			f := v.Elem().FieldByName(names[i])
			if f.CanSet() {
				f.Set(reflect.ValueOf(column).Elem())
			}
		}
		if !isSliceToPointers(rv) {
			v = v.Elem()
		}
		rv.Elem().Set(reflect.Append(rv.Elem(), v))
	}
	return nil
}

func isSliceToPointers(value reflect.Value) bool {
	return value.Elem().Type().Elem().Kind() == reflect.Ptr
}

func (query *StatementBuilder) scannableGeneratedFields(metas ...TableMeta) ([]interface{}, []string) {
	scannable := []interface{}{}
	names := []string{}
	for _, meta := range metas {
		columns := meta.Columns.Iter()
		for columns.Next() {
			_, column := columns.Current()
			if column.Tag.Generated {
				value := reflect.New(column.Type).Interface()
				scannable = append(scannable, value)
				names = append(names, column.GetName())
			}
		}
	}
	return scannable, names
}

func (query *StatementBuilder) scannableColumns(metas ...TableMeta) ([]interface{}, []string) {
	scannable := []interface{}{}
	names := []string{}
	if len(query.pieces.Selects) > 0 {
		for _, sel := range query.pieces.Selects {
			sel = strings.ToLower(sel)
			for _, meta := range metas {
				for i := 0; i < meta.Columns.Size(); i++ {
					_, column := meta.Columns.Index(i)
					metaName := query.qualifyColumn(meta, column.GetName())
					if sel == strings.ToLower(column.Name) || sel == strings.ToLower(column.Tag.Name) || sel == strings.ToLower(metaName) {
						value := reflect.New(column.Type).Interface()
						scannable = append(scannable, value)
						names = append(names, column.GetName())
					}
				}
			}
		}
	} else {
		for _, meta := range metas {
			for i := 0; i < meta.Columns.Size(); i++ {
				_, column := meta.Columns.Index(i)
				value := reflect.New(column.Type).Interface()
				scannable = append(scannable, value)
				names = append(names, column.GetName())
			}
		}
	}
	return scannable, names
}

func (query *StatementBuilder) buildSelects(meta TableMeta, joins []JoinConstraint) []string {
	selects := query.pieces.Selects
	if len(selects) < 1 {
		metas := []TableMeta{meta}
		for _, join := range joins {
			if _, ok := meta.Joins[join.Name]; ok {
				metas = append(metas, join.Export.Meta)
			} else if _, ok := meta.ManyToManyJoins[join.Name]; ok {
				metas = append(metas, join.Export.Meta)
			}
		}
		for _, meta := range metas {
			columns := meta.Columns.Iter()
			for columns.Next() {
				name, field := columns.Current()
				if query.IsValidType(field.Type) {
					column := query.qualifyColumn(meta, name)
					selects = append(selects, column)
				}
			}
		}
	}
	return selects
}

func (query *StatementBuilder) buildSection(prefix, joiner string, constraints []string) {
	wedge := prefix
	for _, constraint := range constraints {
		query.raw.WriteString(wedge)
		wedge = joiner
		query.raw.WriteString(constraint)
	}
}

func (query *StatementBuilder) qualifyColumn(meta TableMeta, name string) string {
	column := name
	if meta.Name != "" {
		column = fmt.Sprintf("%s.%s", meta.Name, name)
		if query.GetSchema() != "" {
			column = fmt.Sprintf("%s.%s", query.GetSchema(), column)
		}
	}
	return column
}

// GetSchema returns the schema to be used within the query being built
func (query *StatementBuilder) GetSchema() string {
	if query.pieces.Schema == "" {
		return query.connection.driver.DefaultSchema()
	}
	return query.pieces.Schema
}

// IsValidType requests for the dialect driver to determine whether the provided type is valid
func (query *StatementBuilder) IsValidType(t reflect.Type) bool {
	return query.connection.driver.IsValidType(t)
}

type Export struct {
	Name string
	Meta TableMeta
}

type JoinConstraint struct {
	Constraint
	Export
	Kind    JoinKind
	From    TableMeta
	Against TableMeta
}

func extractExports(joins []JoinConstraint) []Export {
	exports := []Export{}
	for _, join := range joins {
		if join.Name != "" {
			exports = append(exports, join.Export)
		}
	}
	return exports
}
