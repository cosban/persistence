package statement

import (
	"bytes"
	"encoding/gob"
	"hash/fnv"
)

// Statement represents a prepared statement. Statements may be constructed by calling Prepare.
type Statement interface {
	Query() string
	Args() []interface{}
	ToExec() bool
	Hash() uint64
}

type statement struct {
	query  string
	args   []interface{}
	toExec bool
}

// New returns a new Statement
func New(query string, args []interface{}, toExec bool) Statement {
	return statement{
		query:  query,
		args:   args,
		toExec: toExec,
	}
}

func (s statement) Query() string {
	return s.query
}

func (s statement) Args() []interface{} {
	return s.args
}

func (s statement) ToExec() bool {
	return s.toExec
}

// Hash creates a uint64 representation of the statement
func (s statement) Hash() uint64 {
	hasher := fnv.New64a()
	hasher.Write([]byte(s.query))
	for _, arg := range s.args {
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		err := enc.Encode(arg)
		if err != nil {
			panic(err)
		}
		hasher.Write(buf.Bytes())
	}
	return hasher.Sum64()
}
