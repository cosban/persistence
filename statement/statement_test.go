package statement

import (
	"testing"

	"github.com/cosban/assert"
)

func TestHashEquality(t *testing.T) {
	assert := assert.New(t)

	first := New("test", []interface{}{1}, false).Hash()
	second := New("test", []interface{}{1}, false).Hash()

	assert.NotEquals(0, first)
	assert.Equals(first, second)
}
