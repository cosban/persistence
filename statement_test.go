package persistence

import (
	"reflect"
	"testing"

	"github.com/cosban/assert"
)

func TestIsSliceField(t *testing.T) {
	type example struct {
		Int      int
		Slice    []int
		SlicePtr *[]int
		PtrSlice []*int
	}
	given := reflect.TypeOf(example{})
	t.Run("TestIntNotSlice", func(t *testing.T) {
		field, _ := given.FieldByName("Int")
		actual := isSliceField(field)
		assert.False(t, actual)
	})
	t.Run("TestSliceIsSlice", func(t *testing.T) {
		field, _ := given.FieldByName("Slice")
		actual := isSliceField(field)
		assert.True(t, actual)
	})
	t.Run("TestSlicePtrIsSlice", func(t *testing.T) {
		field, _ := given.FieldByName("SlicePtr")
		actual := isSliceField(field)
		assert.True(t, actual)
	})
	t.Run("TestPtrSliceIsSlice", func(t *testing.T) {
		field, _ := given.FieldByName("PtrSlice")
		actual := isSliceField(field)
		assert.True(t, actual)
	})
}

func TestRepresentsSameField(t *testing.T) {
	type example struct {
		Int      int
		Slice    []int
		SlicePtr *[]int
		PtrSlice []*int
	}
	model := reflect.TypeOf(example{})
	t.Run("TestMustBeSlice", func(t *testing.T) {
		field, _ := model.FieldByName("Int")
		var given int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.False(t, actual)
	})
	t.Run("TestSlices", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
	t.Run("TestSlicePtrs", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
	t.Run("TestPtrSlices", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
	t.Run("TestSlicesW/Ptr", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given *int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
	t.Run("TestSlicePtrsW/Ptr", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given *int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
	t.Run("TestPtrSlicesW/Ptr", func(t *testing.T) {
		field, _ := model.FieldByName("Slice")
		var given *int
		reflected := reflect.TypeOf(given)
		actual := representSameType(field, reflected)
		assert.True(t, actual)
	})
}
