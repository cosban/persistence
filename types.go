package persistence

import (
	"reflect"
)

// GetUsableType takes in an interface and returns a "usable" `reflect.Type` which represents it. A type is considered
// "usable" if it is not a pointer. If a pointer is passed to `GetUsableType`, then the method will return a
// `reflect.Type` of the underlying item.
func GetUsableType(i interface{}) reflect.Type {
	in := reflect.TypeOf(i)
	return getUsableType(in)
}

func getUsableType(t reflect.Type) reflect.Type {
	if t.Kind() == reflect.Ptr {
		t = getUsableType(t.Elem())
	}
	return t
}

// GetUsableValue takes in an interface and returns a "usable" `reflect.Value` which represents it. A value is
// considered "usable" if it is not a pointer. If a pointer is passed to `GetUsableValue`, then the method will return
// a `reflect.Value` of the underlying item. If that pointer is nil, a zero-value of that type will be returned.
func GetUsableValue(i interface{}) reflect.Value {
	in := reflect.ValueOf(i)
	return getUsableValue(in)
}

func getUsableValue(value reflect.Value) reflect.Value {
	if value.Kind() == reflect.Ptr {
		if value.IsNil() {
			value = reflect.New(value.Type().Elem())
		}
		value = getUsableValue(value.Elem())
	}
	return value
}

// GetValueFrom takes in a `reflect.Value` and returns an actual implementation of that value
func GetValueFrom(value reflect.Value) interface{} {
	if value.CanInterface() {
		return value.Interface()
	}
	switch value.Kind() {
	case reflect.Ptr:
		if value.IsNil() {
			return nil
		}
		return GetValueFrom(value.Elem())
	case reflect.Bool:
		return value.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return value.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return value.Uint()
	case reflect.Float32, reflect.Float64:
		return value.Float()
	case reflect.Complex64, reflect.Complex128:
		return value.Complex()
	case reflect.String:
		return value.String()
	}
	return nil
}
