package persistence

import (
	"reflect"
	"testing"
	"time"

	"github.com/cosban/assert"
)

func TestGetValueofNil(t *testing.T) {
	expected := (*uint)(nil)
	given := struct {
		Field *uint
	}{
		Field: expected,
	}

	testGetValue(t, given, expected)
}

func TestGetValueFromExportedField(t *testing.T) {
	expected := "ExpoRted"
	given := struct {
		Field string
	}{
		Field: expected,
	}

	testGetValue(t, given, expected)
}

func TestGetValueFromNonExportedField(t *testing.T) {
	help := "help"
	now := time.Now()
	expected := []interface{}{
		"test",
		13.4,
		true,
		now,
		help,
	}

	given := struct {
		stringField string
		floatField  float64
		boolField   bool
		TimeField   time.Time
		strPtrField *string
	}{
		stringField: "test",
		floatField:  13.4,
		boolField:   true,
		TimeField:   now,
		strPtrField: &help,
	}
	testGetValue(t, given, expected...)
}

func testGetValue(t *testing.T, given interface{}, expected ...interface{}) {
	t.Helper()
	assert := assert.New(t)

	value := reflect.ValueOf(given)

	for i, expect := range expected {
		actual := GetValueFrom(value.Field(i))
		assert.Equals(expect, actual)
	}
}
